/**
 * Created by chen on 2016/12/6.
 * 邮箱配置模块
 */
layui.define(['layer','jquery'], function(exports){
    var $ = layui.jquery //末尾不要加分号 ";"
        ,layer = layui.layer

    var vm = new Vue({
        el: '#content',
        data: {
            send_name:"",
            smtp:"",
            send_account : "",
            send_password : "",
            init_id : ''
        },
        ready : function() {
            var load = layer.load(2);
            var _this = this;
            //获取数据,进行绑定
            $.ajax({
                url: '/em-find_list.html',
                data: {} ,
                method: "POST",
                async : true,
                dataType : "json",
                success: function (data) {
                    layer.close(load);
                    if (!data.error) {
                        _this.send_name = data.data.send_name;
                        _this.smtp = data.data.smtp;
                        _this.send_account = data.data.send_account;
                        _this.send_password = data.data.send_password;
                        _this.init_id = data.id;
                    }
                }
            });
        },
        methods:{
            sub_data:function(){
                var load = layer.load(2);
                $.ajax({
                    url: '/a-email-conf.html',
                    data: {
                        "send_name": this.send_name,
                        "smtp": this.smtp,
                        "send_account": this.send_account,
                        "send_password": this.send_password,
                        "init_id" : this.init_id
                    } ,
                    method: "POST",
                    async : true,
                    dataType : "json",
                    success: function (data) {
                        layer.close(load);
                        layer.msg(data.msg,{},function(){
                            window.location.reload();
                        });
                    }
                });
            }
        }
    });
    exports('email_conf', {});
});