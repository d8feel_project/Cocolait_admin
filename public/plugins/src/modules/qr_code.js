/**
 * Created by chen on 2017/1/10.
 * Qrcode二维码模块
 */
layui.define(['layer','jquery'], function(exports){
    var $ = layui.jquery //末尾不要加分号 ";"
        ,layer = layui.layer
    var _qr_img = $("#qr-img");
    $("#qr-act").on('click',function(){
        _qr_img.attr('src','/qr-act/' + Math.random() + ".html");
    });
    $("#qr-general").on('click',function(){
        _qr_img.attr('src','/qr-general/' + Math.random() + ".html");
    });
    exports('qr_code', {});
});