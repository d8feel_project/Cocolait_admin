/**
 * Created by chen on 2016/12/14.
 * 短信配置模块
 */
layui.define(['layer','jquery'], function(exports){
    var $ = layui.jquery //末尾不要加分号 ";"
        ,layer = layui.layer

    var vm = new Vue({
        el: '#content',
        data: {
            appkey:"",
            format:"",
            secretKey : "",
            init_id : ""
        },
        ready : function() {
            var load = layer.load(2);
            var _this = this;
            //获取数据,进行绑定
            $.ajax({
                url: '/nt-get-conf.html',
                data: {} ,
                method: "POST",
                async : true,
                dataType : "json",
                success: function (data) {
                    layer.close(load);
                    if (!data.error) {
                        _this.appkey = data.data.appkey;
                        _this.format = data.data.format;
                        _this.secretKey = data.data.secretKey;
                        _this.init_id = data.id;
                    }
                }
            });
        },
        methods:{
            sub_note_data:function(){
                var load = layer.load(2);
                $.ajax({
                    url: '/nt-save-conf.html',
                    data: {
                        "appkey": this.appkey,
                        "format": this.format,
                        "secretKey": this.secretKey,
                        "init_id" : this.init_id
                    } ,
                    method: "POST",
                    async : true,
                    dataType : "json",
                    success: function (data) {
                        layer.close(load);
                        layer.msg(data.msg,{},function(){
                            window.location.reload();
                        });
                    }
                });
            }
        }
    });
    exports('note_index', {});
});