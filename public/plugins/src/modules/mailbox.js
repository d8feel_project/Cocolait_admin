/**
 * Created by chen on 2016/12/6.
 * 注册邮箱模板
 */
layui.define(['layer','jquery'], function(exports){
    var $ = layui.jquery //末尾不要加分号 ";"
        ,layer = layui.layer
    $("#mailbox").on('submit',function(){
        var load = layer.load(2);
        var info_data = $(this).serialize();
        //获取数据,进行绑定
        $.ajax({
            url: '/em-save-mailbox.html',
            data: info_data,
            method: "POST",
            async : true,
            dataType : "json",
            success: function (data) {
                layer.close(load);
                if (!data.error) {
                    layer.msg(data.msg,{},function(){
                        window.location.href = data.url;
                    });
                } else {
                    layer.msg(data.msg);
                }
            }
        });
        return false;
    });
    exports('mailbox', {});
});