/**
 * Created by chen on 2016/12/30.
 */
layui.define(['layer'], function(exports){
    var layer = layui.layer;
    /*重新检测*/
    $("#re-rf").on('click',function(){
        window.location.reload();
    });
    /*环境检查*/
    $("#step2").on('click',function(){
        var _err = $("input[name=err]").val();
        if (_err == 0) {
            window.location.href = "/sp2/" + Math.random()  + ".html";
        } else {
            layer.msg('您没有通过检测,无法进入下一步 ^_^');
        }
    });
    /*上一步*/
    $("#sp1").on('click',function(){
        window.location.href = "/sp1.html";
    });

    /*创建数据*/
    $("#step_form").on('submit',function(){
        var _info = $(this).serialize();
        var load = layer.load(2);
        $.ajax({
            url: '/ajax_sp.html',
            data: _info,
            method: "POST",
            async : true,
            dataType : "json",
            success: function (data) {
                layer.close(load);
                if (!data.error) {
                    layer.msg(data.msg,{},function(){
                        window.location.href = data.url;
                    });
                } else {
                    layer.msg(data.msg);
                }
            }
        });
       return false;
    });
    exports('step', {});
});