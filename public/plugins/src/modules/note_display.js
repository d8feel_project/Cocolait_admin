/**
 * Created by chen on 2016/12/14.
 * 短信配置模块
 */
layui.define(['layer','jquery'], function(exports){
    var $ = layui.jquery //末尾不要加分号 ";"
        ,layer = layui.layer

    var vm = new Vue({
        el: '#content',
        data: {
            free_sign_name:"",
            template_code:"",
            display_name : "",
            scene : "",
            is_status : "",
            init_id : ""
        },
        ready : function() {
            if (this.init_id) {
                var load = layer.load(2);
                var _this = this;
                //获取数据,进行绑定
                $.ajax({
                    url: '/nt-find.html',
                    data: {'id' : _this.init_id} ,
                    method: "POST",
                    async : true,
                    dataType : "json",
                    success: function (data) {
                        layer.close(load);
                        if (!data.error) {
                            _this.free_sign_name = data.data.free_sign_name;
                            _this.template_code = data.data.template_code;
                            _this.display_name = data.data.display_name;
                            _this.scene = data.data.scene;
                        }
                    }
                });
            }
        },
        methods:{
            sub_theme_data:function(){
                var load = layer.load(2);
                $.ajax({
                    url: '/nt-save-theme.html',
                    data: {
                        "free_sign_name": this.free_sign_name,
                        "template_code": this.template_code,
                        "display_name": this.display_name,
                        "scene": this.scene,
                        "is_status": this.is_status,
                        "id" : this.init_id
                    } ,
                    method: "POST",
                    async : true,
                    dataType : "json",
                    success: function (data) {
                        layer.close(load);
                        if (!data.error) {
                            layer.msg(data.msg,{},function(){
                                window.location.href = data.url;
                            });
                        } else {
                            layer.msg(data.msg);
                        }
                    }
                });
            }
        }
    });
    exports('note_display', {});
});