/**
 * Created by chen on 2016/12/6.
 * 初始化获取数据
 */
layui.define(['layer','jquery'], function(exports){
    var $ = layui.jquery //末尾不要加分号 ";"
        ,layer = layui.layer
    var vm = new Vue({
        el: '#sidebar',
        data: {
            cp_intro:"",
            cp_username : "",
            cp_user_img : ""
        },
        ready : function() {
            var load = layer.load(2);
            var _this = this;
            $.ajax({
                url: '/a-list-info.html',
                data: {} ,
                method: "POST",
                async : true,
                dataType : "json",
                success: function (data) {
                    layer.close(load);
                    if (!data.error) {
                        _this.cp_username = data.data.username;
                        $("#user-name").html(_this.cp_username);
                        _this.cp_intro = data.data.signature;
                        if (data.data.face) {
                            _this.cp_user_img = data.data.face;
                            $("#top-user-img").attr('src',_this.cp_user_img);
                        } else {
                            _this.cp_user_img = _public + '/assets/img/user.jpg';
                            $("#top-user-img").attr('src',_this.cp_user_img);
                        }
                    }
                }
            });
        }
    });
    exports('admin', {});
});