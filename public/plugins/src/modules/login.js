/**
 * Created by chen on 2016/12/2.
 * 登录模块
 */
layui.define(['layer','jquery'], function(exports){
    var $ = layui.jquery //末尾不要加分号 ";"
        ,layer = layui.layer

    $(window).keydown(function(event){
        if (event.keyCode == 13){
            $("#login").click();
        }
    });
    var vm = new Vue({
        el: '#page-container',
        data: {
            username:"",
            password:""
        },
        ready : function() {
            $("#login").attr('disabled',true);
        },
        methods:{
            login:function(){
                if(!this.login_btn_valid){
                    // 没有输入账号和密码，禁止点击
                }else{
                    var load = layer.load(2);
                    $.ajax({
                        url: '/a-send_login.html',
                        data: {
                            "username": this.username,
                            "password": this.password
                        } ,
                        method: "POST",
                        async : true,
                        dataType : "json",
                        success: function (data) {
                            layer.close(load);
                            if (!data.error) {
                                window.location.href = data.url;
                            } else {
                                layer.msg(data.msg);
                            }
                        }
                    });
                }
            }
        },
        computed: {
            login_btn_valid: function () {
                // 登录按钮的可点击验证
                if(this.username !="" && this.password != ""){
                    $("#login").attr('disabled',false);
                    return true
                }else{
                    return false
                }
            },
        }
    });
    exports('login', {});
});