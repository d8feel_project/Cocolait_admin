/**
 * Created by chen on 2016/12/6.
 * 个人信息模块
 */
layui.define(['layer','jquery','upload'], function(exports){
    var $ = layui.jquery //末尾不要加分号 ";"
        ,layer = layui.layer
        ,upload = layui.upload

    var vm = new Vue({
        el: '#content',
        data: {
            nickname:"",
            email:"",
            sex : "",
            signature : "",
            old_password : '',
            new_password : "",
            repassword : ""
        },
        ready : function() {
            var load = layer.load(2);
            var _this = this;
            //获取数据,进行绑定
            $.ajax({
                url: '/a-list-info.html',
                data: {} ,
                method: "POST",
                async : true,
                dataType : "json",
                success: function (data) {
                    layer.close(load);
                    if (!data.error) {
                        _this.nickname = data.data.nickname;
                        _this.email = data.data.email;
                        _this.sex = data.data.sex;
                        _this.signature = data.data.signature;
                        $("#hide_img").attr('value',data.data.face);
                        if (data.data.face_240_180) {
                            $("#img-src").attr('src',data.data.face_240_180);
                            $("#img-src").attr('type','1');
                        } else {
                            $("#img-src").attr('src',_public + '/assets/img/gallery/gallery-1.jpg');
                            $("#img-src").attr('type','2');
                        }
                    }
                }
            });
        },
        methods:{
            //修改个人信息
            sub_info:function(){
                var load = layer.load(2);
                $.ajax({
                    url: '/a-save_info.html',
                    data: {
                        "nickname": this.nickname,
                        "email": this.email,
                        "sex": this.sex,
                        "signature": this.signature
                    } ,
                    method: "POST",
                    async : true,
                    dataType : "json",
                    success: function (data) {
                        layer.close(load);
                        if (!data.error) {
                            layer.msg(data.msg,{},function(){
                                window.location.reload();
                            });
                        } else {
                            layer.msg(data.msg);
                        }
                    }
                });
            },
            //修改密码
            sub_edit_pass : function () {
                var load = layer.load(2);
                $.ajax({
                    url: '/a-edit_pwd.html',
                    data: {
                        "old_password": this.old_password,
                        "new_password": this.new_password,
                        "repassword": this.repassword
                    } ,
                    method: "POST",
                    async : true,
                    dataType : "json",
                    success: function (data) {
                        layer.close(load);
                        if (!data.error) {
                            layer.msg(data.msg,{},function(){
                                window.location.href = data.url;
                            });
                        } else {
                            layer.msg(data.msg);
                        }
                    }
                });
            },
            //移除图片
            rm_img : function () {
                var load = layer.load(2);
                var _face = $("#hide_img").val();
                var _face240 = $("#img-src").attr('src');
                var _type = $("#img-src").attr('type');
                if (_type == 2) {
                    layer.close(load);
                    layer.msg('默认图片不可移除哦 ^_^',{time:3000});
                    return false;
                }
                $.ajax({
                    url: '/a-del-img.html',
                    data: {
                        "face": _face,
                        "face_240": _face240
                    } ,
                    method: "POST",
                    async : true,
                    dataType : "json",
                    success: function (data) {
                        layer.close(load);
                        if (!data.error) {
                            layer.msg(data.msg,{},function(){
                                window.location.reload();
                            });
                        } else {
                            layer.msg(data.msg);
                        }
                    }
                });
            }
        }
    });
    var file_loads;
    //图像处理
    upload({
        url: '/a-upload.html' //上传服务器路径
        ,title: '上传头像' //自定义文本
        ,elem: '#up-face' //指定原始元素，默认直接查找class="layui-upload-file"
        ,method: 'post'
        ,ext: 'jpg|png|gif|jpeg' //上传文件后缀
        ,before: function(input){
            //执行上传前的回调
            file_loads = layer.load(2);
        }
        ,success: function(res){
            layer.close(file_loads);
            if (!res.error) {
                layer.msg(res.msg,{icon:6});
                $("#img-src").attr('type',1);
                $("#img-src").attr('src',res.url);
            } else {
                layer.msg(res.msg,{icon:2});
            }
        }
    });
    exports('userinfo', {});
});