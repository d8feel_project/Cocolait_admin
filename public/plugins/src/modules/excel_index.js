/**
 * Created by chen on 2016/12/14.
 * 短信配置模块
 */
layui.define(['layer','jquery','upload'], function(exports){
    var $ = layui.jquery //末尾不要加分号 ";"
        ,layer = layui.layer
        ,upload = layui.upload
    var file_loads;
    //图像处理
    upload({
        url: '/ex-upload.html' //上传服务器路径
        ,title: '上传Excel' //自定义文本
        ,elem: '#excel' //指定原始元素，默认直接查找class="layui-upload-file"
        ,method: 'post'
        ,ext: 'xls|xlsx' //上传文件后缀
        ,before: function(input){
            //执行上传前的回调
            file_loads = layer.load(2);
        }
        ,success: function(res){
            layer.close(file_loads);
            if (!res.error) {
                layer.msg(res.msg,{icon:6},function(){
                    window.location.href = '/ex-list.html';
                });
            } else {
                layer.msg(res.msg,{icon:2});
            }
        }
    });

    exports('excel_index', {});
});