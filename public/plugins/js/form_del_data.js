/**
 * Created by chen on 2016/12/13.
 */
$(function(){
    var _box = $("#maillbox_list");
    _box.on('click',".cp-del-emil-list",function(){
        var _id = $(this).attr('eid');
        var _url = $(this).attr('url');
        var _parnet = $(this).parents(".cp-tr-box");
        var load = layer.load(2);
        //获取数据,进行绑定
        $.ajax({
            url: '/' + _url,
            data: {'id' : _id},
            method: "POST",
            async : true,
            dataType : "json",
            success: function (data) {
                layer.close(load);
                if (!data.error) {
                    layer.msg(data.msg,{},function(){
                        _parnet.fadeOut('slow');
                    });
                } else {
                    layer.msg(data.msg);
                }
            }
        });
    });
});
