# Cocolait后台整合系统

## 链接
- 博客：http://www.mgchen.com （基于全新tp5架构打造）
- github：https://github.com/cocolait 
- oschina：http://git.oschina.net/cocolait

## 简介
一个偶然的想法，想用thinkphp5写一套后台管理系统，把平时自己所学所用整理到这个系统中...也算是给自己整理知识点吧。
后续会逐步去完善。
![Cocolait后台管理系统](http://git.oschina.net/uploads/images/2016/1205/095805_5f507723_588912.png "Cocolait后台管理系统")
![Cocolait后台管理系统](http://git.oschina.net/uploads/images/2016/1220/141657_62ed034d_588912.png "Cocolait后台管理系统")
## 说明
目前还没有体验地址，抱歉 ㄒoㄒ~~。

## 项目介绍

1. vue.js,layui.js搭配使用,实现按需加载文件,数据绑定；
1. 较好的PHPMail邮件系统,实现多场景多模板发送；
1. Alidayu(阿里大鱼)短信管理整合,实现多场景发送;
1. 较好的PHPExcel应用管理,已实现PHPExcel类常用的操作（导入Excel,Excel文件下载,Excel文件转换php数组等等...）;
1. 基于thinkPHP5的第三方登录管理,支持最常用第三方(微信扫码登录,QQ登录,新浪微博登录);
1. QRcode动态生成二维码,可自由生成;
1. 图形拖动验证码验证(极验验证),可快速进行验证;
1. 后续会考虑集成（有我自己基于ThinkPHP v5 + GetewayWorker 构建即时通讯项目）

## PHP版本要求
php >= 5.6 建议使用 php 5.6 或者 php 7.0

## 后台
建议本地服务器配置好虚拟域名,输入域名后进入安装程序填写信息,安装完成后即可进入后台登录页！

## 感激
感谢以下的项目,排名不分先后
- ThinkPHP 5
- layUi.js
- Vue.js