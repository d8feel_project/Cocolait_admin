<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:67:"D:\wamp64\www\web\Cocolait_admin/apps/install\view\index\index.html";i:1486186931;s:69:"D:\wamp64\www\web\Cocolait_admin/apps/install\view\layout\common.html";i:1486186931;s:69:"D:\wamp64\www\web\Cocolait_admin/apps/install\view\public\header.html";i:1486186931;s:69:"D:\wamp64\www\web\Cocolait_admin/apps/install\view\public\footer.html";i:1486186931;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cocolait后台整合系统</title>
    <link href="__PLUGINS__/zui-1.5.0/css/zui.min.css" rel="stylesheet" />
    <link href="__PLUGINS__/layui/css/layui.css" rel="stylesheet">
</head>
<body>

<div class="content" style="margin-top: 30px;">
    <style>
        .nav>li>a{
            padding: 8px 147px;
        }
        .label-badge{
            padding: 3px 5px;
        }
        .wamp{
            box-shadow: 0 0 5px #ccc;
            background: #fff;
            border-radius: 5px;
            padding-right: 0px;
            padding-left: 0px;
        }
    </style>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-7 wamp">
            <div class="alert with-icon alert-success">
                <i class="icon-ok-sign"></i>
                <div class="content"><strong>Cocolait_admin 安装向导!</strong></div>
            </div>
            <ul class="nav nav-secondary">
                <li class="active"><a href="javascript:;"><span class="label label-badge label-success">1</span> 检测环境</a></li>
                <li><a href="javascript:;"><span class="label label-badge">2</span> 创建数据</a></li>
                <li><a href="javascript:;"><span class="label label-badge">3</span> 完成安装</a></li>
            </ul>
            <!--主体内容 START-->
            <div class="example" style="margin-top: 1px;">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>环境检测</th>
                        <th>推荐配置</th>
                        <th>当前状态</th>
                        <th>最低要求</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>操作系统</td>
                        <td>类UNIX</td>
                        <td><?php echo $data['os']; ?></td>
                        <td>不限制</td>
                    </tr>
                    <tr>
                        <td>PHP版本</td>
                        <td>>5.6.x</td>
                        <td><?php echo $data['php_ves']; ?></td>
                        <td>5.6</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="example" style="margin-top: 20px;">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>扩展检测</th>
                        <th>启用状态</th>
                        <th>状态</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>session</td>
                        <?php if($data['is_session']): ?>
                        <td>开启</td>
                        <?php else: ?>
                        <td>关闭</td>
                        <?php endif; ?>
                        <td><?php echo $data['session']; ?></td>
                    </tr>
                    <tr>
                        <td>PDO</td>
                        <?php if($data['is_pdo']): ?>
                        <td>开启</td>
                        <?php else: ?>
                        <td>关闭</td>
                        <?php endif; ?>
                        <td><?php echo $data['pdo']; ?></td>
                    </tr>
                    <tr>
                        <td>PDO_MySQL</td>
                        <?php if($data['is_pdo_mysql']): ?>
                        <td>开启</td>
                        <?php else: ?>
                        <td>关闭</td>
                        <?php endif; ?>
                        <td><?php echo $data['pdo_mysql']; ?></td>
                    </tr>
                    <tr>
                        <td>CURL</td>
                        <?php if($data['is_curl']): ?>
                        <td>开启</td>
                        <?php else: ?>
                        <td>关闭</td>
                        <?php endif; ?>
                        <td><?php echo $data['curl']; ?></td>
                    </tr>
                    <tr>
                        <td>GD</td>
                        <?php if($data['is_gd']): ?>
                        <td>开启</td>
                        <?php else: ?>
                        <td>关闭</td>
                        <?php endif; ?>
                        <td><?php echo $data['gd']; ?></td>
                    </tr>
                    <tr>
                        <td>MBstring</td>
                        <?php if($data['is_mbstring']): ?>
                        <td>开启</td>
                        <?php else: ?>
                        <td>关闭</td>
                        <?php endif; ?>
                        <td><?php echo $data['mbstring']; ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="example" style="margin-top: 20px;">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>目录权限</th>
                        <th>写入</th>
                        <th>读取</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data['folders'] as $k => $v): ?>
                        <tr>
                            <td><?php echo $k; ?></td>
                            <?php if($v['w']): ?>
                            <td><i class="icon icon-check-circle"></i>可写</td>
                            <?php else: ?>
                            <td><i class="icon icon-remove-circle"></i>不可写</td>
                            <?php endif; if($v['r']): ?>
                            <td><i class="icon icon-check-circle"></i>可读</td>
                            <?php else: ?>
                            <td><i class="icon icon-remove-circle"></i>不可读</td>
                            <?php endif; ?>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="btn-group" style="margin-left: 430px;margin-bottom: 10px;">
                <input type="hidden" name="err" value="<?php echo $error; ?>">
                <button type="button" class="btn btn-info" id="re-rf"><i class="icon icon-repeat"></i> 重新检测</button>
                <button type="button" class="btn btn-success" id="step2"><i class="icon icon-share-alt"></i> 下一步</button>
            </div>
            <!--主体内容 END-->
        </div>
        <div class="col-md-3"></div>
    </div>
    <div class="row" style="margin-top: 16px;">
        <div class="col-md-2"></div>
        <div class="col-md-7">
            <span style="margin-left: 428px;">© 2016 Cocolait后台整合系统</span>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>


<script src="__PLUGINS__/zui-1.5.0/jquery-v-3.1.1.min.js"></script>
<script src="__PLUGINS__/zui-1.5.0/js/zui.min.js"></script>
<script src="__PLUGINS__/layui/layui.js"></script>
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('step');
</script>
</body>
</html>