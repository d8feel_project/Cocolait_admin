<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:67:"D:\wamp64\www\web\Cocolait_admin/apps/install\view\index\step3.html";i:1486186931;s:69:"D:\wamp64\www\web\Cocolait_admin/apps/install\view\layout\common.html";i:1486186931;s:69:"D:\wamp64\www\web\Cocolait_admin/apps/install\view\public\header.html";i:1486186931;s:69:"D:\wamp64\www\web\Cocolait_admin/apps/install\view\public\footer.html";i:1486186931;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cocolait后台整合系统</title>
    <link href="__PLUGINS__/zui-1.5.0/css/zui.min.css" rel="stylesheet" />
    <link href="__PLUGINS__/layui/css/layui.css" rel="stylesheet">
</head>
<body>

<div class="content" style="margin-top: 30px;">
    <style>
        .nav>li>a{
            padding: 8px 147px;
        }
        .label-badge{
            padding: 3px 5px;
        }
        .wamp{
            box-shadow: 0 0 5px #ccc;
            background: #fff;
            border-radius: 5px;
            padding-right: 0px;
            padding-left: 0px;
        }
    </style>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-7 wamp">
            <div class="alert with-icon alert-success">
                <i class="icon-ok-sign"></i>
                <div class="content"><strong>Cocolait_admin 安装向导!</strong></div>
            </div>
            <ul class="nav nav-secondary">
                <li><a href="javascript:;"><span class="label label-badge">1</span> 检测环境</a></li>
                <li><a href="javascript:;"><span class="label label-badge">2</span> 创建数据</a></li>
                <li class="active"><a href="javascript:;"><span class="label label-badge label-success">3</span> 完成安装</a></li>
            </ul>
            <!--主体内容 START-->
            <div class="panel-body">
                <div class="alert alert-success with-icon">
                    <i class="icon-ok-sign"></i>
                    <div class="content">
                        <h4>Cocolait_admin项目</h4>
                        <hr>
                        <strong>已安装完毕。</strong>
                    </div>
                </div>
                <div class="alert alert-primary with-icon">
                    <div class="content">
                        <a class="btn btn-success" href="/a-login.html"><i class="icon icon-arrow-right"></i> 进入后台</a>
                    </div>
                </div>
                <form class="form-horizontal" id="step_form">
                   <?php foreach($data as $v): if(!strrpos($v['msg'],'INSERT INTO')): ?>
                    <div class="alert alert-success with-icon">
                        <i class="icon-ok-sign"></i>
                        <div class="content">
                            <h4><?php echo $v['msg']; ?></h4>
                        </div>
                    </div>
                    <?php endif; endforeach; ?>
                </form>
            </div>
            <!--主体内容 END-->
        </div>
        <div class="col-md-3"></div>
    </div>
    <div class="row" style="margin-top: 16px;">
        <div class="col-md-2"></div>
        <div class="col-md-7">
            <span style="margin-left: 428px;">© 2016 Cocolait后台整合系统</span>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>


<script src="__PLUGINS__/zui-1.5.0/jquery-v-3.1.1.min.js"></script>
<script src="__PLUGINS__/zui-1.5.0/js/zui.min.js"></script>
<script src="__PLUGINS__/layui/layui.js"></script>
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('step');
</script>
</body>
</html>