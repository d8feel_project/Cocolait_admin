<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:67:"D:\wamp64\www\web\Cocolait_admin/apps/install\view\index\step2.html";i:1486186931;s:69:"D:\wamp64\www\web\Cocolait_admin/apps/install\view\layout\common.html";i:1486186931;s:69:"D:\wamp64\www\web\Cocolait_admin/apps/install\view\public\header.html";i:1486186931;s:69:"D:\wamp64\www\web\Cocolait_admin/apps/install\view\public\footer.html";i:1486186931;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cocolait后台整合系统</title>
    <link href="__PLUGINS__/zui-1.5.0/css/zui.min.css" rel="stylesheet" />
    <link href="__PLUGINS__/layui/css/layui.css" rel="stylesheet">
</head>
<body>

<div class="content" style="margin-top: 30px;">
    <style>
        .nav>li>a{
            padding: 8px 147px;
        }
        .label-badge{
            padding: 3px 5px;
        }
        .wamp{
            box-shadow: 0 0 5px #ccc;
            background: #fff;
            border-radius: 5px;
            padding-right: 0px;
            padding-left: 0px;
        }
    </style>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-7 wamp">
            <div class="alert with-icon alert-success">
                <i class="icon-ok-sign"></i>
                <div class="content"><strong>Cocolait_admin 安装向导!</strong></div>
            </div>
            <ul class="nav nav-secondary">
                <li><a href="javascript:;"><span class="label label-badge">1</span> 检测环境</a></li>
                <li class="active"><a href="javascript:;"><span class="label label-badge label-success">2</span> 创建数据</a></li>
                <li><a href="javascript:;"><span class="label label-badge">3</span> 完成安装</a></li>
            </ul>
            <!--主体内容 START-->
            <div class="panel-body">
                <form class="form-horizontal" id="step_form">
                    <div class="alert alert-primary with-icon">
                        <i class="icon-star"></i>
                        <div class="content">
                            <h4>数据库配置</h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">数据库服务器</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" placeholder="一般为 127.0.0.1" name="hostname">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">数据库名</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" placeholder="" name="database">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">数据库端口</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" placeholder="3306" name="hostport">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">数据库用户名</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" placeholder="root" name="username">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">数据库密码</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" placeholder="" name="password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">数据库表前缀</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" placeholder="cp_" name="prefix">
                        </div>
                    </div>

                    <div class="alert alert-primary with-icon">
                        <i class="icon-star"></i>
                        <div class="content">
                            <h4>创建管理员</h4>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">管理员帐号</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" placeholder="输入管理员帐号" name="uname">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">密码</label>
                        <div class="col-md-3">
                            <input type="password" class="form-control" placeholder="输入密码" name="upwd">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">重复密码</label>
                        <div class="col-md-3">
                            <input type="password" class="form-control" placeholder="输入重复密码" name="re_upwd">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Email</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" placeholder="输入确认密码" name="email">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-9">
                            <button type="button" class="btn btn-info" id="sp1"><i class="icon icon-arrow-left"></i> 上一步</button>
                            <button type="submit" class="btn btn-success"><i class="icon icon-checked"></i> 创建数据</button>
                        </div>
                    </div>
                </form>
            </div>
            <!--主体内容 END-->
        </div>
        <div class="col-md-3"></div>
    </div>
    <div class="row" style="margin-top: 16px;">
        <div class="col-md-2"></div>
        <div class="col-md-7">
            <span style="margin-left: 428px;">© 2016 Cocolait后台整合系统</span>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>


<script src="__PLUGINS__/zui-1.5.0/jquery-v-3.1.1.min.js"></script>
<script src="__PLUGINS__/zui-1.5.0/js/zui.min.js"></script>
<script src="__PLUGINS__/layui/layui.js"></script>
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('step');
</script>
</body>
</html>