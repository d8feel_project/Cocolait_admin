<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:35:"./template/admin/qrcodes\index.html";i:1506491488;s:35:"./template/admin/layout\common.html";i:1506491488;s:35:"./template/admin/public\header.html";i:1506491488;s:37:"./template/admin/public\top_menu.html";i:1506491488;s:38:"./template/admin/public\left_menu.html";i:1506491488;s:35:"./template/admin/public\footer.html";i:1506491488;}*/ ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title><?php echo config('cocolait'); ?></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

     <!--================== 必须加载的样式 ==================-->
    <link href="__PUBLIC__/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/css/style.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/css/animate.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/css/theme/default.css" rel="stylesheet" id="theme" />
    <link href="__PLUGINS__/layui/css/layui.css" rel="stylesheet">
    <!-- ================== END 必须加载的样式 ================== -->

    <!-- ================== 表单样式 ================== -->
    <link href="__PUBLIC__/assets/plugins/DataTables/css/data-table.css" rel="stylesheet" />
    <!-- ================== END 表单样式 ================== -->

    <!--图片展示 插件样式 START-->
    <link href="__PUBLIC__/assets/plugins/isotope/isotope.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/plugins/lightbox/css/lightbox.css" rel="stylesheet" />
    <!--图片展示 插件样式 END-->

    <!--按钮选择 样式 START-->
    <link href="__PUBLIC__/assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/plugins/powerange/powerange.min.css" rel="stylesheet" />
    <!--按钮选择 样式 END-->

    <!-- ================== 刷新加载进度条 插件  START================== -->
    <script src="__PUBLIC__/assets/plugins/pace/pace.min.js"></script>
    <!-- ================== 刷新加载进度条 插件  END ================== -->
    <script>
        var _public = "__PUBLIC__";
    </script>

</head>
<body id="controller">
<div id="page-loader" class="fade"><span class="spinner"></span></div>

<!-- begin #page-container -->
<div id="page-container" class="fade in page-sidebar-fixed page-header-fixed">

	<!--加载顶部菜单栏-->
	<!--顶部菜单栏-->
<!-- begin #header -->
<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="index.html" class="navbar-brand"><span class="navbar-logo"></span>Cocolait</a>
            <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end mobile sidebar expand / collapse button -->

        <!-- begin header navigation right -->
        <ul class="nav navbar-nav navbar-right">
            <li>
                <form class="navbar-form full-width">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter keyword" />
                        <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
                    </div>
                </form>
            </li>
            <li class="dropdown">
                <a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14">
                    <i class="fa fa-bell-o"></i>
                    <span class="label">5</span>
                </a>
                <ul class="dropdown-menu media-list pull-right animated fadeInDown">
                    <li class="dropdown-header">Notifications (5)</li>
                    <li class="media">
                        <a href="javascript:;">
                            <div class="media-left"><i class="fa fa-bug media-object bg-red"></i></div>
                            <div class="media-body">
                                <h6 class="media-heading">Server Error Reports</h6>
                                <div class="text-muted f-s-11">3 minutes ago</div>
                            </div>
                        </a>
                    </li>
                    <li class="media">
                        <a href="javascript:;">
                            <div class="media-left"><img src="__PUBLIC__/assets/img/user-1.jpg" class="media-object" alt=""/></div>
                            <div class="media-body">
                                <h6 class="media-heading">John Smith</h6>
                                <p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
                                <div class="text-muted f-s-11">25 minutes ago</div>
                            </div>
                        </a>
                    </li>
                    <li class="media">
                        <a href="javascript:;">
                            <div class="media-left"><img src="__PUBLIC__/assets/img/user-2.jpg" class="media-object" alt="" /></div>
                            <div class="media-body">
                                <h6 class="media-heading">Olivia</h6>
                                <p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
                                <div class="text-muted f-s-11">35 minutes ago</div>
                            </div>
                        </a>
                    </li>
                    <li class="media">
                        <a href="javascript:;">
                            <div class="media-left"><i class="fa fa-plus media-object bg-green"></i></div>
                            <div class="media-body">
                                <h6 class="media-heading"> New User Registered</h6>
                                <div class="text-muted f-s-11">1 hour ago</div>
                            </div>
                        </a>
                    </li>
                    <li class="media">
                        <a href="javascript:;">
                            <div class="media-left"><i class="fa fa-envelope media-object bg-blue"></i></div>
                            <div class="media-body">
                                <h6 class="media-heading"> New Email From John</h6>
                                <div class="text-muted f-s-11">2 hour ago</div>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-footer text-center">
                        <a href="javascript:;">View more</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown navbar-user">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="" id="top-user-img" />
                    <span class="hidden-xs" id="user-name"></span> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu animated fadeInLeft">
                    <li class="arrow"></li>
                    <li><a href="javascript:;">修改个人信息</a></li>
                    <li><a href="javascript:;"><span class="badge badge-danger pull-right">2</span> 邮件</a></li>
                    <li><a href="javascript:;">日历</a></li>
                    <li><a href="javascript:;">设置</a></li>
                    <li class="divider"></li>
                    <li><a href="/a-loginOut.html">退出</a></li>
                </ul>
            </li>
        </ul>
        <!-- end header navigation right -->
    </div>
    <!-- end container-fluid -->
</div>
<!-- end #header -->

	<!--加载左侧菜单-->
	<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                <div class="image">
                    <a href="javascript:;"><img v-bind:src="cp_user_img" alt=""/></a>
                </div>
                <div class="info">
                    <span>{{cp_username}}</span>
                    <small>{{cp_intro}}</small>
                </div>
            </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
            <li class="nav-header">左侧菜单栏</li>
            <?php foreach($adminMenu as $v): ?>
            <li <?php if(\think\Request::instance()->module() == $v['module'] && \think\Request::instance()->controller() == $v['controller']): ?>class="<?php echo $v['open']; ?>"<?php else: ?>class="<?php echo $v['close']; ?>"<?php endif; ?>>
                <?php foreach($v['live_2'] as $v2): ?>
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="<?php echo $v2['ico']; ?>"></i>
                    <span><?php echo $v2['name']; ?> <span class="label label-theme m-l-5"><?php echo $v2['newName']; ?></span></span>
                </a>
                <ul class="sub-menu <?php if(\think\Request::instance()->action() == $v2['action'] && \think\Request::instance()->controller() == $v2['controller']): ?>active<?php endif; ?>">
                    <?php if($v2['live_3']): foreach($v2['live_3'] as $v3): ?>
                    <li <?php if(\think\Request::instance()->action() == $v3['action'] && \think\Request::instance()->controller() == $v3['controller']): ?>class="active"<?php endif; ?>><a href="<?php echo $v3['url']; ?>"><?php echo $v3['name']; ?></a></li>
                    <?php endforeach; endif; ?>
                </ul>
                <?php endforeach; ?>
            </li>
            <?php endforeach; ?>

            <!--左侧收缩按钮 START -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify" title="点击收缩"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- 左侧收缩按钮 END -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->

	<!-- 主体内容区域 START -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<?php foreach($crumbs as $v): if($v['item']): ?>
			<li class="active"><?php echo $v['name']; ?></li>
			<?php else: ?>
			<li><a href="<?php echo $v['url']; ?>"><?php echo $v['name']; ?></a></li>
			<?php endif; endforeach; ?>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">QRcode动态生成</h1>
		<!-- end page-header -->

		<!-- begin row -->
		<div class="row">
			<!-- begin col-6 -->
			<div class="col-md-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="1.html#default-tab-1" data-toggle="tab">QRcode动态生成</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade active in" id="default-tab-1">
						<ul class="result-list">
							<li style="box-shadow:none;">
								<div class="result-image" style="width:240px;height: 240px;">
									<img src="/qr-act.html"  id="qr-img">
								</div>
								<div style="padding-top: 75px;">
									<button class="layui-btn layui-btn-primary" id="qr-act">动态生成带LOGO的二维码</button>
									<button class="layui-btn layui-btn-primary" id="qr-general">动态生成普通二维码</button>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- end col-6 -->

		</div>
		<!-- end row -->
	</div>
	<!-- 主体内容区域 END -->

	<!-- 回到顶部 按钮 STATR-->
	<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
	<!-- 回到顶部 按钮 END -->
</div>
<!-- end page container -->

<!-- ================== 必须加载的JS START ================== -->
<script src="__PUBLIC__/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="__PUBLIC__/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="__PUBLIC__/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="__PUBLIC__/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="__PUBLIC__/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="__PLUGINS__/layui/layui.js"></script>
<script type="text/javascript" src="__PLUGINS__/vue/vue.js"></script>
<!-- ================== 必须加载的JS END ================== -->

<!--按钮选择插件 START-->
<script src="__PUBLIC__/assets/plugins/switchery/switchery.min.js"></script>
<script src="__PUBLIC__/assets/plugins/powerange/powerange.min.js"></script>
<script src="__PUBLIC__/assets/js/form-slider-switcher.demo.min.js"></script>
<!--按钮选择插件 end-->

<!--初始化加载的js文件-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('admin');
</script>
<script src="__PUBLIC__/assets/js/apps.min.js"></script>
<script>
    $(document).ready(function() {
        App.init();
        //按钮状态 选择初始化
        FormSliderSwitcher.init();
    });
</script>
<!--初始化加载的js文件 END-->


<!-- ================== 其他应用的JS START ================== -->
<?php if(\think\Request::instance()->controller() == 'Setting'): ?>
<!--加载设置模块-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('userinfo');
</script>
<?php endif; if(\think\Request::instance()->controller() == 'Email' && \think\Request::instance()->action() == 'index'): ?>
<!--加载邮箱设置模块-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('email_conf');
</script>
<?php endif; if(\think\Request::instance()->controller() == 'Email' && \think\Request::instance()->action() == 'mailbox'): ?>
<!--加载邮箱设置模块-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('mailbox');
</script>
<?php endif; ?>

<!--邮箱模板列表加载表单插件-->
<?php if((\think\Request::instance()->controller() == 'Email' && \think\Request::instance()->action() == 'mailbox_list')
|| (\think\Request::instance()->controller() == 'Note' && \think\Request::instance()->action() == 'note_list')
|| (\think\Request::instance()->controller() == 'Excel' && \think\Request::instance()->action() == 'excel_list')): ?>
<!--表单插件 START-->
<script src="__PLUGINS__/js/form_del_data.js"></script>
<script src="__PUBLIC__/assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="__PUBLIC__/assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="__PUBLIC__/assets/js/table-manage-responsive.demo.min.js"></script>
<!--表单插件 END-->
<script>
    $(document).ready(function() {
        //初始化表单插件
        TableManageResponsive.init();
    });
</script>
<?php endif; if(\think\Request::instance()->controller() == 'Note' && \think\Request::instance()->action() == 'index'): ?>
<!--加载短信设置模块-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('note_index');
</script>
<?php elseif(\think\Request::instance()->controller() == 'Note' && \think\Request::instance()->action() == 'note_display'): ?>
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('note_display');
</script>
<?php endif; if(\think\Request::instance()->controller() == 'Excel' && \think\Request::instance()->action() == 'index'): ?>
<!--加载Eexcl应用模块-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('excel_index');
</script>
<?php endif; if(\think\Request::instance()->controller() == 'Qrcodes'): ?>
<!--加载QRcode二维码-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('qr_code');
</script>
<?php endif; ?>
<!-- ================== 其他应用的JS END ================== -->
</body>
</html>