<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:33:"./template/admin/login\index.html";i:1506491488;s:35:"./template/admin/layout\common.html";i:1506491488;s:35:"./template/admin/public\header.html";i:1506491488;s:35:"./template/admin/public\footer.html";i:1506491488;}*/ ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title><?php echo config('cocolait'); ?></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

     <!--================== 必须加载的样式 ==================-->
    <link href="__PUBLIC__/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/css/style.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/css/animate.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/css/theme/default.css" rel="stylesheet" id="theme" />
    <link href="__PLUGINS__/layui/css/layui.css" rel="stylesheet">
    <!-- ================== END 必须加载的样式 ================== -->

    <!-- ================== 表单样式 ================== -->
    <link href="__PUBLIC__/assets/plugins/DataTables/css/data-table.css" rel="stylesheet" />
    <!-- ================== END 表单样式 ================== -->

    <!--图片展示 插件样式 START-->
    <link href="__PUBLIC__/assets/plugins/isotope/isotope.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/plugins/lightbox/css/lightbox.css" rel="stylesheet" />
    <!--图片展示 插件样式 END-->

    <!--按钮选择 样式 START-->
    <link href="__PUBLIC__/assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/plugins/powerange/powerange.min.css" rel="stylesheet" />
    <!--按钮选择 样式 END-->

    <!-- ================== 刷新加载进度条 插件  START================== -->
    <script src="__PUBLIC__/assets/plugins/pace/pace.min.js"></script>
    <!-- ================== 刷新加载进度条 插件  END ================== -->
    <script>
        var _public = "__PUBLIC__";
    </script>

</head>
<body id="controller">
<div id="page-loader" class="fade"><span class="spinner"></span></div>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title><?php echo config('cocolait'); ?></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

     <!--================== 必须加载的样式 ==================-->
    <link href="__PUBLIC__/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/css/style.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/css/animate.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/css/theme/default.css" rel="stylesheet" id="theme" />
    <link href="__PLUGINS__/layui/css/layui.css" rel="stylesheet">
    <!-- ================== END 必须加载的样式 ================== -->

    <!-- ================== 表单样式 ================== -->
    <link href="__PUBLIC__/assets/plugins/DataTables/css/data-table.css" rel="stylesheet" />
    <!-- ================== END 表单样式 ================== -->

    <!--图片展示 插件样式 START-->
    <link href="__PUBLIC__/assets/plugins/isotope/isotope.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/plugins/lightbox/css/lightbox.css" rel="stylesheet" />
    <!--图片展示 插件样式 END-->

    <!--按钮选择 样式 START-->
    <link href="__PUBLIC__/assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
    <link href="__PUBLIC__/assets/plugins/powerange/powerange.min.css" rel="stylesheet" />
    <!--按钮选择 样式 END-->

    <!-- ================== 刷新加载进度条 插件  START================== -->
    <script src="__PUBLIC__/assets/plugins/pace/pace.min.js"></script>
    <!-- ================== 刷新加载进度条 插件  END ================== -->
    <script>
        var _public = "__PUBLIC__";
    </script>

</head>
<body id="controller">
<div id="page-loader" class="fade"><span class="spinner"></span></div>

	<div class="login-cover">
	    <div class="login-cover-image"><img src="__PUBLIC__/assets/img/login-bg/bg-1.jpg" data-id="login-cover-image" alt="" /></div>
	    <div class="login-cover-bg"></div>
	</div>
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
	    <!-- begin login -->
        <div class="login login-v2" data-pageload-addclass="animated flipInX">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <span class="logo"></span> Ccocolait管理系统
                    <small>Ccocolait管理系统 登录页</small>
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            <!-- end brand -->
            <div class="login-content">
                <form class="margin-bottom-0">
                    <div class="form-group m-b-20">
                        <input type="text" class="form-control input-lg" placeholder="用户名" v-model="username"/>
                    </div>
                    <div class="form-group m-b-20">
                        <input type="password" class="form-control input-lg" placeholder="密码" v-model="password"/>
                    </div>

                    <div class="login-buttons">
                        <button type="button" class="btn btn-block btn-lg"
                                :class={"btn-s":!login_btn_valid,"btn-success":login_btn_valid}
                                @click.prevent="login" id="login">登 录</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- end login -->
        
        <ul class="login-bg-list">
            <li class="active"><a href="javascript:;" data-click="change-bg"><img src="__PUBLIC__/assets/img/login-bg/bg-1.jpg" alt="" /></a></li>
            <li><a href="javascript:;" data-click="change-bg"><img src="__PUBLIC__/assets/img/login-bg/bg-2.jpg" alt="" /></a></li>
            <li><a href="javascript:;" data-click="change-bg"><img src="__PUBLIC__/assets/img/login-bg/bg-3.jpg" alt="" /></a></li>
            <li><a href="javascript:;" data-click="change-bg"><img src="__PUBLIC__/assets/img/login-bg/bg-4.jpg" alt="" /></a></li>
            <li><a href="javascript:;" data-click="change-bg"><img src="__PUBLIC__/assets/img/login-bg/bg-5.jpg" alt="" /></a></li>
        </ul>
        
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="__PUBLIC__/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="__PUBLIC__/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="__PUBLIC__/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="__PUBLIC__/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- ================== END BASE JS ================== -->

	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="__PUBLIC__/assets/js/login-v2.demo.min.js"></script>
	<script src="__PUBLIC__/assets/js/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
    <script type="text/javascript" src="__PLUGINS__/layui/layui.js"></script>
    <script type="text/javascript" src="__PLUGINS__/vue/vue.js"></script>
	<script>
		$(document).ready(function() {
			App.init();
			LoginV2.init();
		});
        layui.config({
            base: '__PLUGINS__/src/modules/' //你的模块目录
        }).use('login'); //加载文件
	</script>
</body>
</html>

<!-- ================== 必须加载的JS START ================== -->
<script src="__PUBLIC__/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="__PUBLIC__/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="__PUBLIC__/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="__PUBLIC__/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="__PUBLIC__/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="__PLUGINS__/layui/layui.js"></script>
<script type="text/javascript" src="__PLUGINS__/vue/vue.js"></script>
<!-- ================== 必须加载的JS END ================== -->

<!--按钮选择插件 START-->
<script src="__PUBLIC__/assets/plugins/switchery/switchery.min.js"></script>
<script src="__PUBLIC__/assets/plugins/powerange/powerange.min.js"></script>
<script src="__PUBLIC__/assets/js/form-slider-switcher.demo.min.js"></script>
<!--按钮选择插件 end-->

<!--初始化加载的js文件-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('admin');
</script>
<script src="__PUBLIC__/assets/js/apps.min.js"></script>
<script>
    $(document).ready(function() {
        App.init();
        //按钮状态 选择初始化
        FormSliderSwitcher.init();
    });
</script>
<!--初始化加载的js文件 END-->


<!-- ================== 其他应用的JS START ================== -->
<?php if(\think\Request::instance()->controller() == 'Setting'): ?>
<!--加载设置模块-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('userinfo');
</script>
<?php endif; if(\think\Request::instance()->controller() == 'Email' && \think\Request::instance()->action() == 'index'): ?>
<!--加载邮箱设置模块-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('email_conf');
</script>
<?php endif; if(\think\Request::instance()->controller() == 'Email' && \think\Request::instance()->action() == 'mailbox'): ?>
<!--加载邮箱设置模块-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('mailbox');
</script>
<?php endif; ?>

<!--邮箱模板列表加载表单插件-->
<?php if((\think\Request::instance()->controller() == 'Email' && \think\Request::instance()->action() == 'mailbox_list')
|| (\think\Request::instance()->controller() == 'Note' && \think\Request::instance()->action() == 'note_list')
|| (\think\Request::instance()->controller() == 'Excel' && \think\Request::instance()->action() == 'excel_list')): ?>
<!--表单插件 START-->
<script src="__PLUGINS__/js/form_del_data.js"></script>
<script src="__PUBLIC__/assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="__PUBLIC__/assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="__PUBLIC__/assets/js/table-manage-responsive.demo.min.js"></script>
<!--表单插件 END-->
<script>
    $(document).ready(function() {
        //初始化表单插件
        TableManageResponsive.init();
    });
</script>
<?php endif; if(\think\Request::instance()->controller() == 'Note' && \think\Request::instance()->action() == 'index'): ?>
<!--加载短信设置模块-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('note_index');
</script>
<?php elseif(\think\Request::instance()->controller() == 'Note' && \think\Request::instance()->action() == 'note_display'): ?>
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('note_display');
</script>
<?php endif; if(\think\Request::instance()->controller() == 'Excel' && \think\Request::instance()->action() == 'index'): ?>
<!--加载Eexcl应用模块-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('excel_index');
</script>
<?php endif; if(\think\Request::instance()->controller() == 'Qrcodes'): ?>
<!--加载QRcode二维码-->
<script>
    layui.config({
        base: '__PLUGINS__/src/modules/'
    }).use('qr_code');
</script>
<?php endif; ?>
<!-- ================== 其他应用的JS END ================== -->
</body>
</html>