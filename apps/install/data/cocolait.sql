/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50714
Source Host           : 127.0.0.1:3306
Source Database       : cocolait

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2016-12-20 14:23:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cp_admin
-- ----------------------------
DROP TABLE IF EXISTS `cp_admin`;
CREATE TABLE `cp_admin` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` char(32) NOT NULL COMMENT '密码',
  `email` varchar(60) NOT NULL DEFAULT '' COMMENT '邮箱',
  `face` varchar(100) NOT NULL DEFAULT '' COMMENT '用户头像',
  `sex` enum('男','女') NOT NULL DEFAULT '男' COMMENT '性别',
  `signature` varchar(100) NOT NULL DEFAULT '' COMMENT '个性签名',
  `last_login_ip` char(16) NOT NULL DEFAULT '0.0.0.0',
  `last_login_time` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '最后的登录时间',
  `is_lock` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否锁定 1：锁定 0：正常',
  `pwd_error_num` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '密码错误次数 10次锁定',
  `nickname` varchar(30) NOT NULL DEFAULT '' COMMENT '昵称',
  `face_240_180` varchar(100) NOT NULL DEFAULT '' COMMENT '头像240x180',
  PRIMARY KEY (`uid`),
  KEY `is_lock` (`is_lock`,`pwd_error_num`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='后台用户表';



-- ----------------------------
-- Table structure for cp_article
-- ----------------------------
DROP TABLE IF EXISTS `cp_article`;
CREATE TABLE `cp_article` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `excerpt` varchar(200) NOT NULL DEFAULT '' COMMENT '文章摘要',
  `content` longtext NOT NULL COMMENT '文章内容',
  `img` varchar(150) NOT NULL DEFAULT '' COMMENT '文章图片路径',
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '文章标题',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0:未审核 1：已审核',
  `comment_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否允许评论 1：允许 0：不允许',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文章添加时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文章更新时间',
  `hits` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击数（查看数）',
  `is_top` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否置顶 1：置顶 0：不置顶',
  `praise` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '点赞数',
  `recommended` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否推荐 1：推荐 0：不推荐',
  `post_source` varchar(150) NOT NULL DEFAULT '' COMMENT '转载文章的来源',
  `post_keywords` varchar(150) NOT NULL DEFAULT '' COMMENT '搜索文章的关键字',
  `users_uid` int(11) NOT NULL COMMENT '发布者的ID',
  `is_recycle` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否回收 1：回收  0：不回收',
  `cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类ID',
  PRIMARY KEY (`aid`),
  KEY `fk_cp_article_cp_users1_idx` (`users_uid`),
  KEY `cid` (`cid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of cp_article
-- ----------------------------

-- ----------------------------
-- Table structure for cp_article_tags
-- ----------------------------
DROP TABLE IF EXISTS `cp_article_tags`;
CREATE TABLE `cp_article_tags` (
  `cp_tags_tid` int(10) unsigned NOT NULL,
  `cp_article_aid` int(10) unsigned NOT NULL,
  KEY `fk_table2_cp_article1_idx` (`cp_article_aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文章和标签的中中间关联表';

-- ----------------------------
-- Records of cp_article_tags
-- ----------------------------

-- ----------------------------
-- Table structure for cp_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `cp_auth_group`;
CREATE TABLE `cp_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '' COMMENT '用户组中文名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'status 状态：为1正常，为0禁用',
  `rules` varchar(200) NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id， 多个规则","隔开',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户组表';

-- ----------------------------
-- Records of cp_auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for cp_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `cp_auth_group_access`;
CREATE TABLE `cp_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL COMMENT '后台用户ID',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '用户组ID',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户和用户组的中间表';

-- ----------------------------
-- Records of cp_auth_group_access
-- ----------------------------

-- ----------------------------
-- Table structure for cp_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `cp_auth_rule`;
CREATE TABLE `cp_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一标识 列如：Admin/User/index 模块/控制器/方法',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父类规则ID',
  `condition` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='规则表';

-- ----------------------------
-- Records of cp_auth_rule
-- ----------------------------

-- ----------------------------
-- Table structure for cp_category
-- ----------------------------
DROP TABLE IF EXISTS `cp_category`;
CREATE TABLE `cp_category` (
  `cid` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `name` varchar(60) NOT NULL COMMENT '分类名称',
  `type` varchar(60) NOT NULL COMMENT '分类类型',
  `description` varchar(200) NOT NULL COMMENT '分类描述',
  `parent` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '分类父id',
  `count` int(11) NOT NULL DEFAULT '0' COMMENT '分类文章数',
  `order` smallint(6) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用 1：启用 0：不启用',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='文章分类表';

-- ----------------------------
-- Records of cp_category
-- ----------------------------

-- ----------------------------
-- Table structure for cp_check_email
-- ----------------------------
DROP TABLE IF EXISTS `cp_check_email`;
CREATE TABLE `cp_check_email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` char(40) NOT NULL DEFAULT '' COMMENT '发送的邮箱号码',
  `sub_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送验证码的时间',
  `code` char(6) NOT NULL DEFAULT '' COMMENT '验证码 （目前字符串类型 php写入时记得转换类型）',
  `is_check` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否验证 1：已验证  0：未验证',
  `check_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '验证时间 验证完成时写入',
  `check_item` varchar(100) NOT NULL DEFAULT '' COMMENT '验证的唯一标识 邮箱号码_验证码_验证场景标识',
  `scene` varchar(50) NOT NULL DEFAULT '' COMMENT '发送邮箱验证码的场景注释',
  PRIMARY KEY (`id`),
  KEY `is_check` (`is_check`,`check_item`),
  KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='邮箱验证码_记录_验证表';

-- ----------------------------
-- Records of cp_check_email
-- ----------------------------
INSERT INTO `cp_check_email` VALUES ('62', '401498334@qq.com', '1481698796', '897413', '0', '0', '401498334@qq.com_897413_subEmailCode', '邮箱发送验证码');
INSERT INTO `cp_check_email` VALUES ('63', '401498334@qq.com', '1481698939', '671024', '0', '0', '401498334@qq.com_671024_subEmailCode', '邮箱发送验证码');

-- ----------------------------
-- Table structure for cp_check_email_sublink
-- ----------------------------
DROP TABLE IF EXISTS `cp_check_email_sublink`;
CREATE TABLE `cp_check_email_sublink` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(50) NOT NULL DEFAULT '' COMMENT '邮件激活token',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `state` varchar(30) NOT NULL DEFAULT '' COMMENT '说明',
  `url` varchar(300) NOT NULL DEFAULT '' COMMENT '激活地址',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否激活 1：已激活 0：未激活',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送时间',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型  1邮箱注册 2：邮箱找回密码 3:绑定邮箱',
  PRIMARY KEY (`id`),
  KEY `email_key` (`token`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=1596 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cp_check_email_sublink
-- ----------------------------
INSERT INTO `cp_check_email_sublink` VALUES ('1595', 'e0e644c5e58910218e0a3c9e7c21c76f', '1', '邮箱注册', 'http://www.cms.com/h-check-email/e0e644c5e58910218e0a3c9e7c21c76f/1.html', '1', '1481620497', '1');

-- ----------------------------
-- Table structure for cp_check_phone
-- ----------------------------
DROP TABLE IF EXISTS `cp_check_phone`;
CREATE TABLE `cp_check_phone` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` char(15) NOT NULL DEFAULT '' COMMENT '发送的手机号码',
  `sub_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送验证码的时间',
  `code` char(8) NOT NULL DEFAULT '' COMMENT '验证码 （目前字符串类型 php写入时记得转换类型）',
  `is_check` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否验证 1：已验证  0：未验证',
  `check_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '验证时间 验证完成时写入',
  `check_item` varchar(100) NOT NULL DEFAULT '' COMMENT '验证的唯一标识 手机号码_验证码_验证场景标识',
  `note_display_name` varchar(50) NOT NULL DEFAULT '' COMMENT '短信模板名称',
  `is_del` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '双重验证已通过 0：表示没有双重验证通过  1：表示已通过此条记录可删除也可保留',
  PRIMARY KEY (`id`),
  KEY `phone` (`phone`,`is_check`,`check_item`)
) ENGINE=MyISAM AUTO_INCREMENT=236 DEFAULT CHARSET=utf8 COMMENT='H5端手机验证码_记录_验证表';

-- ----------------------------
-- Records of cp_check_phone
-- ----------------------------

-- ----------------------------
-- Table structure for cp_comments
-- ----------------------------
DROP TABLE IF EXISTS `cp_comments`;
CREATE TABLE `cp_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL COMMENT '原文地址',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '发表评论的用户id',
  `to_uid` int(11) NOT NULL DEFAULT '0' COMMENT '被评论的用户id',
  `full_name` varchar(50) DEFAULT NULL COMMENT '评论者昵称',
  `email` varchar(255) DEFAULT NULL COMMENT '评论者邮箱',
  `createtime` datetime NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '评论时间',
  `content` text NOT NULL COMMENT '评论内容',
  `parentid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '被回复的评论id  父类ID 0为最高级',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '状态，1已审核，0未审核',
  `aid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文章id',
  `post_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上一次评论的id',
  PRIMARY KEY (`id`),
  KEY `comment_approved_date_gmt` (`status`),
  KEY `comment_parent` (`parentid`),
  KEY `table_id_status` (`status`),
  KEY `createtime` (`createtime`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='评论表';

-- ----------------------------
-- Records of cp_comments
-- ----------------------------

-- ----------------------------
-- Table structure for cp_common_action_log
-- ----------------------------
DROP TABLE IF EXISTS `cp_common_action_log`;
CREATE TABLE `cp_common_action_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` bigint(20) DEFAULT '0' COMMENT '用户id',
  `object` varchar(100) DEFAULT NULL COMMENT '访问对象的id,格式：不带前缀的表名+id;如posts1表示xx_posts表里id为1的记录',
  `action` varchar(50) DEFAULT NULL COMMENT '操作名称；格式规定为：应用名+控制器+操作名；也可自己定义格式只要不发生冲突且惟一；',
  `count` int(11) DEFAULT '0' COMMENT '访问次数',
  `last_time` int(11) DEFAULT '0' COMMENT '最后访问的时间戳',
  `ip` varchar(15) DEFAULT NULL COMMENT '访问者最后访问ip',
  PRIMARY KEY (`id`),
  KEY `user_object_action` (`user`,`object`,`action`),
  KEY `user_object_action_ip` (`user`,`object`,`action`,`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='访问记录表';

-- ----------------------------
-- Records of cp_common_action_log
-- ----------------------------

-- ----------------------------
-- Table structure for cp_email_display
-- ----------------------------
DROP TABLE IF EXISTS `cp_email_display`;
CREATE TABLE `cp_email_display` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `display_name` varchar(64) NOT NULL COMMENT '模板名称',
  `content` text NOT NULL COMMENT '模板内容 存json数据数据',
  `is_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启 1：开启 0：关闭',
  PRIMARY KEY (`id`,`is_status`),
  UNIQUE KEY `display_name` (`display_name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='邮箱模板';

-- ----------------------------
-- Records of cp_email_display
-- ----------------------------
INSERT INTO `cp_email_display` VALUES ('10', '邮箱发送验证码', '{\"title\":\"Cocolait\\u535a\\u5ba2\",\"content\":\"<table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\"><tbody><tr class=\\\"firstRow\\\"><td height=\\\"60\\\" width=\\\"829\\\" style=\\\"font-size: 12px; -webkit-font-smoothing: subpixel-antialiased; background-color: rgb(142, 193, 218); text-indent: 10px; line-height: 60px; word-break: break-all;\\\"><span style=\\\"color: rgb(255, 255, 255);\\\"><span style=\\\"font-size: 28px;\\\"><strong>Cocolait\\u535a\\u5ba2<\\/strong><\\/span><\\/span><\\/td><\\/tr><tr><td width=\\\"829\\\" style=\\\"font-size: 12px; -webkit-font-smoothing: subpixel-antialiased; padding-top: 25px; padding-bottom: 22px; word-break: break-all;\\\"><p style=\\\"line-height: 20.4px;\\\"><span style=\\\"font-size: 18px;\\\">HI\\uff0c<\\/span><span style=\\\"font-size: 18px; text-decoration: underline; color: rgb(79, 129, 189);\\\">#username#<\\/span><span style=\\\"font-size: 18px;\\\">\\uff1a<\\/span><\\/p><p style=\\\"line-height: 20.4px;\\\"><br\\/><\\/p><p style=\\\"line-height: 20.4px;\\\"><span style=\\\"font-size: 18px;\\\">\\u6b22\\u8fce\\u4f7f\\u7528Cocolait\\u535a\\u5ba2\\u90ae\\u7bb1\\u786e\\u8ba4\\u529f\\u80fd\\u3002<\\/span><\\/p><p style=\\\"line-height: 20.4px;\\\"><br\\/><\\/p><p style=\\\"line-height: 20.4px;\\\"><span style=\\\"font-size: 18px;\\\">\\u60a8\\u672c\\u6b21\\u64cd\\u4f5c\\u6240\\u751f\\u6210\\u7684\\u786e\\u8ba4\\u7801\\u4e3a<\\/span>\\uff1a<span style=\\\"font-size: 24px; text-decoration: underline; color: rgb(38, 38, 38);\\\"><span t=\\\"7\\\" data=\\\"066370\\\" isout=\\\"1\\\" style=\\\"border-bottom: 1px dashed rgb(204, 204, 204); z-index: 1; position: static;\\\">#code#<\\/span><\\/span><\\/p><p style=\\\"line-height: 20.4px;\\\"><span style=\\\"font-size: 24px;\\\"><br\\/><\\/span><\\/p><p style=\\\"line-height: 20.4px;\\\">\\u8bf7\\u572830\\u5206\\u949f\\u5185\\u4f7f\\u7528\\u8be5\\u786e\\u8ba4\\u7801\\uff0c\\u8fc7\\u671f\\u540e\\u60a8\\u9700\\u8981\\u91cd\\u65b0\\u53d1\\u9001\\u9a8c\\u8bc1\\u90ae\\u4ef6\\u83b7\\u53d6\\u786e\\u8ba4\\u7801\\u3002\\u5982\\u679c\\u60a8\\u5e76\\u672a\\u53d1\\u8fc7\\u6b64\\u8bf7\\u6c42\\uff0c\\u5219\\u53ef\\u80fd\\u662f\\u56e0\\u4e3a\\u5176\\u4ed6\\u7528\\u6237\\u5728\\u64cd\\u4f5c\\u65f6\\u8bef\\u8f93\\u5165\\u4e86\\u60a8\\u7684\\u7535\\u5b50\\u90ae\\u4ef6\\u5730\\u5740\\u800c\\u4f7f\\u60a8\\u6536\\u5230\\u8fd9\\u5c01\\u90ae\\u4ef6\\uff0c\\u90a3\\u4e48\\u60a8\\u53ef\\u4ee5\\u653e\\u5fc3\\u7684\\u5ffd\\u7565\\u6b64\\u90ae\\u4ef6\\uff0c\\u65e0\\u9700\\u8fdb\\u4e00\\u6b65\\u91c7\\u53d6\\u4efb\\u4f55\\u64cd\\u4f5c\\u3002<\\/p><\\/td><\\/tr><tr><td height=\\\"161\\\" width=\\\"829\\\" style=\\\"font-size: 12px; -webkit-font-smoothing: subpixel-antialiased; text-align: center; padding: 10px 0px 30px; line-height: 1.5; color: rgb(117, 154, 170); word-break: break-all;\\\"><p style=\\\"line-height: 20.4px;\\\">------------------------------<wbr\\/>------------------------------<wbr\\/>------------------------------<wbr\\/>----------------------<br\\/>\\u6b64\\u4fe1\\u7531Cocolait\\u535a\\u5ba2\\u7cfb\\u7edf\\u53d1\\u51fa\\uff0c\\u7cfb\\u7edf\\u4e0d\\u63a5\\u6536\\u56de\\u4fe1\\uff0c\\u56e0\\u6b64\\u8bf7\\u52ff\\u76f4\\u63a5\\u56de\\u590d\\u3002<br\\/>\\u5982\\u6709\\u4efb\\u4f55\\u7591\\u95ee\\uff0c\\u8bf7\\u8bbf\\u95ee\\u5de5\\u7a0b\\u6613&nbsp;<a href=\\\"http:\\/\\/www.gcy168.com\\/\\\" target=\\\"_blank\\\" style=\\\"outline: none; cursor: pointer; color: rgb(149, 179, 215);\\\"><span style=\\\"outline: none; cursor: pointer; color: rgb(79, 129, 189);\\\">http:\\/\\/<\\/span><\\/a><wbr\\/><a href=\\\"http:\\/\\/www.mgchen.com\\/\\\" target=\\\"_blank\\\" textvalue=\\\"www.mgchen.com\\\" style=\\\"color: rgb(79, 129, 189); text-decoration: underline;\\\"><span style=\\\"color: rgb(79, 129, 189);\\\">www.mgchen.com<\\/span><\\/a>&nbsp;\\u4e0e\\u6211\\u4eec\\u53d6\\u5f97\\u8054\\u7cfb\\u3002<\\/p><\\/td><\\/tr><\\/tbody><\\/table><p><br\\/><\\/p>\"}', '1');
INSERT INTO `cp_email_display` VALUES ('11', '邮箱发送链接', '{\"title\":\"Cocolait\\u535a\\u5ba2\\u90ae\\u4ef6\\u901a\\u77e5\",\"content\":\"<table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\"><tbody><tr class=\\\"firstRow\\\"><td height=\\\"60\\\" width=\\\"829\\\" style=\\\"font-size: 12px; -webkit-font-smoothing: subpixel-antialiased; background-color: rgb(142, 193, 218); text-indent: 10px; line-height: 60px; word-break: break-all;\\\"><span style=\\\"color: rgb(255, 255, 255);\\\"><span style=\\\"font-size: 28px;\\\"><strong>Cocolait\\u535a\\u5ba2<\\/strong><\\/span><\\/span><\\/td><\\/tr><tr><td width=\\\"829\\\" style=\\\"padding-top: 25px; padding-bottom: 22px; font-size: 12px; -webkit-font-smoothing: subpixel-antialiased; word-break: break-all;\\\"><p style=\\\"line-height: 20.4px;\\\"><span style=\\\"font-size: 18px;\\\"><\\/span>\\u672c\\u90ae\\u4ef6\\u6765\\u81ea<a href=\\\"http:\\/\\/www.mgchen.com\\\" target=\\\"_blank\\\" title=\\\"http:\\/\\/www.mgchen.com\\\"><span style=\\\"color: rgb(79, 129, 189); text-decoration: underline;\\\">Cocolait\\u535a\\u5ba2<\\/span><\\/a><br style=\\\"white-space: normal;\\\"\\/><br style=\\\"white-space: normal;\\\"\\/>&nbsp; &nbsp;<strong style=\\\"white-space: normal;\\\">---------------<strong>---<\\/strong><\\/strong><br style=\\\"white-space: normal;\\\"\\/>&nbsp; &nbsp;<strong style=\\\"white-space: normal;\\\">#ext_title#<\\/strong><br style=\\\"white-space: normal;\\\"\\/>&nbsp; &nbsp;<strong style=\\\"white-space: normal;\\\">---------------<strong>---<\\/strong><\\/strong><br style=\\\"white-space: normal;\\\"\\/>&nbsp; &nbsp;<span style=\\\"font-size: 14px;\\\"> \\u5c0a\\u656c\\u7684<span style=\\\"font-family: Arial; color: rgb(51, 51, 51); line-height: 18px; background-color: rgb(255, 255, 255);\\\">#username#\\uff0c\\u60a8\\u597d\\u3002<\\/span>\\u5982\\u679c\\u60a8\\u662fThinkCMF\\u7684\\u65b0\\u7528\\u6237\\uff0c\\u6216\\u5728\\u4fee\\u6539\\u60a8\\u7684\\u6ce8\\u518cEmail\\u65f6\\u4f7f\\u7528\\u4e86\\u672c\\u5730\\u5740\\uff0c\\u6211\\u4eec\\u9700\\u8981\\u5bf9\\u60a8\\u7684\\u5730\\u5740\\u6709\\u6548\\u6027\\u8fdb\\u884c\\u9a8c\\u8bc1\\u4ee5\\u907f\\u514d\\u5783\\u573e\\u90ae\\u4ef6\\u6216\\u5730\\u5740\\u88ab\\u6ee5\\u7528\\u3002<br style=\\\"white-space: normal;\\\"\\/>&nbsp; &nbsp; \\u60a8\\u53ea\\u9700\\u70b9\\u51fb\\u4e0b\\u9762\\u7684\\u94fe\\u63a5\\u5373\\u53ef\\u6fc0\\u6d3b\\u60a8\\u7684\\u5e10\\u53f7\\uff1a<br style=\\\"white-space: normal;\\\"\\/>&nbsp; &nbsp;&nbsp;<\\/span><a href=\\\"http:\\/\\/#link#\\\" target=\\\"_self\\\">http:\\/\\/#link#<\\/a><br style=\\\"white-space: normal;\\\"\\/><span style=\\\"font-size: 14px;\\\">&nbsp; &nbsp; (\\u5982\\u679c\\u4e0a\\u9762\\u4e0d\\u662f\\u94fe\\u63a5\\u5f62\\u5f0f\\uff0c\\u8bf7\\u5c06\\u8be5\\u5730\\u5740\\u624b\\u5de5\\u7c98\\u8d34\\u5230\\u6d4f\\u89c8\\u5668\\u5730\\u5740\\u680f\\u518d\\u8bbf\\u95ee)<br style=\\\"white-space: normal;\\\"\\/>&nbsp; &nbsp; \\u611f\\u8c22\\u60a8\\u7684\\u8bbf\\u95ee\\uff0c\\u795d\\u60a8\\u4f7f\\u7528\\u6109\\u5feb\\uff01<\\/span><br style=\\\"white-space: normal;\\\"\\/><br style=\\\"white-space: normal;\\\"\\/>&nbsp; &nbsp; <span style=\\\"font-size: 14px;\\\">\\u6b64\\u81f4<br style=\\\"white-space: normal;\\\"\\/>&nbsp; &nbsp;Cocolait\\u535a\\u5ba2<\\/span><\\/p><\\/td><\\/tr><tr><td height=\\\"161\\\" width=\\\"829\\\" style=\\\"padding: 10px 0px 30px; font-size: 12px; -webkit-font-smoothing: subpixel-antialiased; text-align: center; line-height: 1.5; color: rgb(117, 154, 170); word-break: break-all;\\\"><p style=\\\"line-height: 20.4px;\\\">------------------------------<wbr\\/>------------------------------<wbr\\/>------------------------------<wbr\\/>----------------------<br\\/>\\u6b64\\u4fe1\\u7531Cocolait\\u535a\\u5ba2\\u7cfb\\u7edf\\u53d1\\u51fa\\uff0c\\u7cfb\\u7edf\\u4e0d\\u63a5\\u6536\\u56de\\u4fe1\\uff0c\\u56e0\\u6b64\\u8bf7\\u52ff\\u76f4\\u63a5\\u56de\\u590d\\u3002<br\\/>\\u5982\\u6709\\u4efb\\u4f55\\u7591\\u95ee\\uff0c\\u8bf7\\u8bbf\\u95ee\\u5de5\\u7a0b\\u6613&nbsp;<a href=\\\"http:\\/\\/www.gcy168.com\\/\\\" target=\\\"_blank\\\" style=\\\"outline: none; cursor: pointer; color: rgb(149, 179, 215);\\\"><span style=\\\"outline: none; cursor: pointer; color: rgb(79, 129, 189);\\\">http:\\/\\/<\\/span><\\/a><wbr\\/><a href=\\\"http:\\/\\/www.mgchen.com\\/\\\" target=\\\"_blank\\\" textvalue=\\\"www.mgchen.com\\\" style=\\\"color: rgb(79, 129, 189);\\\">www.mgchen.com<\\/a>&nbsp;\\u4e0e\\u6211\\u4eec\\u53d6\\u5f97\\u8054\\u7cfb\\u3002<\\/p><\\/td><\\/tr><\\/tbody><\\/table><p><br\\/><\\/p>\"}', '1');
INSERT INTO `cp_email_display` VALUES ('12', '邮箱发送评论回复', '{\"title\":\"Cocolait\\u535a\\u5ba2\\u90ae\\u4ef6\\u901a\\u77e5\",\"content\":\"<table cellpadding=\\\"0\\\" cellspacing=\\\"0\\\"><tbody><tr class=\\\"firstRow\\\"><td height=\\\"60\\\" width=\\\"829\\\" style=\\\"font-size: 12px; -webkit-font-smoothing: subpixel-antialiased; background-color: rgb(142, 193, 218); text-indent: 10px; line-height: 60px; word-break: break-all;\\\"><span style=\\\"color: rgb(255, 255, 255);\\\"><span style=\\\"font-size: 28px;\\\"><strong>Cocolait\\u535a\\u5ba2<\\/strong><\\/span><\\/span><\\/td><\\/tr><tr><td width=\\\"829\\\" style=\\\"padding-top: 25px; padding-bottom: 22px; font-size: 12px; -webkit-font-smoothing: subpixel-antialiased; word-break: break-all;\\\"><p style=\\\"line-height: 20.4px;\\\"><span style=\\\"font-size: 18px;\\\"><\\/span><\\/p><p style=\\\"line-height: 23.8px; font-family: &quot;lucida Grande&quot;, Verdana, &quot;Microsoft YaHei&quot;; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);\\\">\\u672c\\u90ae\\u4ef6\\u6765\\u81ea<a href=\\\"http:\\/\\/www.mgchen.com\\/\\\" target=\\\"_blank\\\" title=\\\"Cocolait\\u535a\\u5ba2\\\"><span style=\\\"text-decoration: underline;\\\">Cocolait\\u535a\\u5ba2<\\/span><\\/a><br\\/><br\\/>&nbsp; &nbsp;<strong>--------------<\\/strong><br\\/>&nbsp; &nbsp; &nbsp;<strong>\\u8bc4\\u8bba\\u56de\\u590d<\\/strong><br\\/>&nbsp; &nbsp;<strong>--------------<\\/strong><br\\/><br\\/>&nbsp; &nbsp; \\u5c0a\\u656c\\u7684#username#\\uff0c\\u60a8\\u597d\\u3002<\\/p><p style=\\\"line-height: 23.8px; font-family: &quot;lucida Grande&quot;, Verdana, &quot;Microsoft YaHei&quot;; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);\\\">&nbsp; &nbsp; &nbsp;<span style=\\\"color: rgb(0, 0, 0);\\\">#to_name#<\\/span>&nbsp;\\u5728<span style=\\\"color: rgb(0, 0, 0);\\\"><strong>\\u300a#article_title#\\u300b<\\/strong><\\/span>\\u6587\\u7ae0\\u4e2d\\u8bc4\\u8bba\\u8bf4\\u9053&nbsp;<span style=\\\"color: rgb(183, 221, 232);\\\">@#<span style=\\\"font-family: &quot;lucida Grande&quot;, Verdana, &quot;Microsoft YaHei&quot;; font-size: 14px; background-color: rgb(255, 255, 255);\\\">username<\\/span>#<\\/span>&nbsp;\\uff1a<\\/p><p style=\\\"line-height: 23.8px; font-family: &quot;lucida Grande&quot;, Verdana, &quot;Microsoft YaHei&quot;; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);\\\">&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; \\u8bc4\\u8bba\\u5185\\u5bb9 \\uff1a #article_content#<\\/p><p style=\\\"line-height: 23.8px; font-family: &quot;lucida Grande&quot;, Verdana, &quot;Microsoft YaHei&quot;; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);\\\">&nbsp; &nbsp; &nbsp;&nbsp;\\u70b9\\u51fb\\u4e0b\\u9762\\u5730\\u5740\\u67e5\\u770b\\u8be6\\u7ec6\\u4fe1\\u606f\\uff1a<\\/p><p style=\\\"line-height: 23.8px; font-family: &quot;lucida Grande&quot;, Verdana, &quot;Microsoft YaHei&quot;; font-size: 14px; white-space: normal; background-color: rgb(255, 255, 255);\\\">&nbsp; &nbsp; &nbsp; <span style=\\\"text-decoration: underline; color: rgb(141, 179, 226);\\\">#link#<\\/span><br\\/>&nbsp; &nbsp; \\u6b64\\u81f4<br\\/>&nbsp; &nbsp; Cocolait\\u535a\\u5ba2<\\/p><\\/td><\\/tr><tr><td height=\\\"161\\\" width=\\\"829\\\" style=\\\"padding: 10px 0px 30px; font-size: 12px; -webkit-font-smoothing: subpixel-antialiased; text-align: center; line-height: 1.5; color: rgb(117, 154, 170); word-break: break-all;\\\"><p style=\\\"line-height: 20.4px;\\\">------------------------------<wbr\\/>------------------------------<wbr\\/>------------------------------<wbr\\/>----------------------<br\\/>\\u6b64\\u4fe1\\u7531Cocolait\\u535a\\u5ba2\\u7cfb\\u7edf\\u53d1\\u51fa\\uff0c\\u7cfb\\u7edf\\u4e0d\\u63a5\\u6536\\u56de\\u4fe1\\uff0c\\u56e0\\u6b64\\u8bf7\\u52ff\\u76f4\\u63a5\\u56de\\u590d\\u3002<br\\/>\\u5982\\u6709\\u4efb\\u4f55\\u7591\\u95ee\\uff0c\\u8bf7\\u8bbf\\u95ee\\u5de5\\u7a0b\\u6613&nbsp;<a href=\\\"http:\\/\\/www.gcy168.com\\/\\\" target=\\\"_blank\\\" style=\\\"outline: none; cursor: pointer; color: rgb(149, 179, 215);\\\"><span style=\\\"outline: none; cursor: pointer; color: rgb(79, 129, 189);\\\">http:\\/\\/<\\/span><\\/a><wbr\\/><a href=\\\"http:\\/\\/www.mgchen.com\\/\\\" target=\\\"_blank\\\" textvalue=\\\"www.mgchen.com\\\" style=\\\"color: rgb(79, 129, 189);\\\">www.mgchen.com<\\/a>&nbsp;\\u4e0e\\u6211\\u4eec\\u53d6\\u5f97\\u8054\\u7cfb\\u3002<\\/p><\\/td><\\/tr><\\/tbody><\\/table><p><br\\/><\\/p>\"}', '1');

-- ----------------------------
-- Table structure for cp_excel
-- ----------------------------
DROP TABLE IF EXISTS `cp_excel`;
CREATE TABLE `cp_excel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT 'execl文件上传名称',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件上传时间',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '下载url',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='ececl管理表';

-- ----------------------------
-- Records of cp_excel
-- ----------------------------
INSERT INTO `cp_excel` VALUES ('2', 'bankCode.xls', '1482119843', './uploads/excel/20161219/bankCode.xls');
INSERT INTO `cp_excel` VALUES ('3', '1482119849_bankCode.xls', '1482119850', './uploads/excel/20161219/1482119849_bankCode.xls');
INSERT INTO `cp_excel` VALUES ('4', '1482119903_bankCode.xls', '1482119903', './uploads/excel/20161219/1482119903_bankCode.xls');
INSERT INTO `cp_excel` VALUES ('5', 'ban.xls', '1482119959', './uploads/excel/20161219/ban.xls');

-- ----------------------------
-- Table structure for cp_guestbook
-- ----------------------------
DROP TABLE IF EXISTS `cp_guestbook`;
CREATE TABLE `cp_guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(50) NOT NULL COMMENT '留言者姓名',
  `email` varchar(100) NOT NULL COMMENT '留言者邮箱',
  `title` varchar(255) DEFAULT NULL COMMENT '留言标题',
  `msg` text NOT NULL COMMENT '留言内容',
  `createtime` datetime NOT NULL COMMENT '留言时间',
  `status` smallint(2) NOT NULL DEFAULT '1' COMMENT '留言状态，1：正常，0：删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='留言表';

-- ----------------------------
-- Records of cp_guestbook
-- ----------------------------

-- ----------------------------
-- Table structure for cp_load_file
-- ----------------------------
DROP TABLE IF EXISTS `cp_load_file`;
CREATE TABLE `cp_load_file` (
  `id` smallint(6) NOT NULL,
  `style_url` text NOT NULL COMMENT '需要加载的文件的路劲',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '加载类型 1：css 2:js 3:ico',
  `is_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否启用 1：启用 0：不启用',
  PRIMARY KEY (`id`),
  KEY `is_status` (`is_status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='加载文件表';

-- ----------------------------
-- Records of cp_load_file
-- ----------------------------

-- ----------------------------
-- Table structure for cp_oauth_user
-- ----------------------------
DROP TABLE IF EXISTS `cp_oauth_user`;
CREATE TABLE `cp_oauth_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(20) NOT NULL DEFAULT '' COMMENT '用户来源key',
  `name` varchar(45) NOT NULL DEFAULT '' COMMENT '第三方昵称',
  `head_img` varchar(100) NOT NULL DEFAULT '' COMMENT '第三方用户头像',
  `create_time` datetime NOT NULL COMMENT '绑定时间',
  `last_login_time` datetime NOT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(16) NOT NULL DEFAULT '0.0.0.0' COMMENT '最后登录ip',
  `login_times` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `status` tinyint(2) unsigned NOT NULL,
  `access_token` varchar(512) NOT NULL COMMENT '用户登录的TOken',
  `expires_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'access_token过期时间',
  `openid` varchar(45) DEFAULT '' COMMENT '第三方用户id',
  `users_uid` int(11) NOT NULL COMMENT '关联的本站用户id',
  PRIMARY KEY (`id`),
  KEY `fk_cp_oauth_user_cp_users_idx` (`users_uid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='第三方用户表';

-- ----------------------------
-- Records of cp_oauth_user
-- ----------------------------

-- ----------------------------
-- Table structure for cp_options
-- ----------------------------
DROP TABLE IF EXISTS `cp_options`;
CREATE TABLE `cp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL COMMENT '配置名',
  `option_value` longtext NOT NULL COMMENT '配置值',
  `autoload` int(2) NOT NULL DEFAULT '1' COMMENT '是否自动加载',
  `intro` varchar(200) NOT NULL DEFAULT '' COMMENT '说明',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='配置表';

-- ----------------------------
-- Table structure for cp_praise
-- ----------------------------
DROP TABLE IF EXISTS `cp_praise`;
CREATE TABLE `cp_praise` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_ip` varchar(100) NOT NULL DEFAULT '' COMMENT '点赞的用户ip',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点赞时间',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '点赞类型 0：游客 1：本站用户',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `aid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文章id',
  `valid_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '有效时间 本站用户保留一小时 游客保留一天',
  `nums` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '点击次数',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`,`aid`) USING BTREE,
  KEY `aid` (`aid`),
  KEY `valid_time` (`valid_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='点赞记录表';

-- ----------------------------
-- Records of cp_praise
-- ----------------------------

-- ----------------------------
-- Table structure for cp_route
-- ----------------------------
DROP TABLE IF EXISTS `cp_route`;
CREATE TABLE `cp_route` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '路由id',
  `full_url` varchar(255) DEFAULT NULL COMMENT '完整url， 如：portal/list/index?id=1',
  `url` varchar(255) DEFAULT NULL COMMENT '实际显示的url',
  `listorder` int(5) DEFAULT '0' COMMENT '排序，优先级，越小优先级越高',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，1：启用 ;0：不启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='url路由表';

-- ----------------------------
-- Records of cp_route
-- ----------------------------

-- ----------------------------
-- Table structure for cp_send_note
-- ----------------------------
DROP TABLE IF EXISTS `cp_send_note`;
CREATE TABLE `cp_send_note` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `free_sign_name` char(30) NOT NULL DEFAULT '' COMMENT '短信签名 注意：必须是审核通过的',
  `template_code` varchar(50) NOT NULL DEFAULT '' COMMENT '短信模板ID，传入的模板必须是在阿里大于“管理中心-短信模板管理”中的可用模板。示例：SMS_585014',
  `scene` varchar(50) NOT NULL DEFAULT '' COMMENT '验证场景标识 驼峰法命名 列如：UserPasswordUpdate',
  `display_name` char(30) NOT NULL DEFAULT '' COMMENT '短信模板名称',
  `is_status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '启用状态 1：正常 0：关闭',
  PRIMARY KEY (`id`),
  UNIQUE KEY `display_name` (`display_name`),
  UNIQUE KEY `scene` (`scene`),
  UNIQUE KEY `template_code` (`template_code`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='阿里大鱼短信模板配置';

-- ----------------------------
-- Records of cp_send_note
-- ----------------------------
INSERT INTO `cp_send_note` VALUES ('1', 'Cocolait', 'SMS_16580265', 'register', '注册短信', '1');
INSERT INTO `cp_send_note` VALUES ('2', 'cocolait', 'SMS_16530262', 'login', '登录短信', '1');
INSERT INTO `cp_send_note` VALUES ('3', 'cocolait', 'SMS_16575190', 'repassword', '找回密码', '1');
INSERT INTO `cp_send_note` VALUES ('4', 'cocolait', 'SMS_18365553', 'binMobile', '绑定手机号码', '1');
INSERT INTO `cp_send_note` VALUES ('5', 'cocolait', 'SMS_18225652', 'editPassword', '修改登录密码', '1');
INSERT INTO `cp_send_note` VALUES ('6', 'cooclait', 'SMS_18460006', 'editUserPaypassword', '设置支付密码', '1');
INSERT INTO `cp_send_note` VALUES ('7', '工程易', 'SMS_25090018', 'UserPasswordUpdate', '登录密码更新提醒', '1');

-- ----------------------------
-- Table structure for cp_tags
-- ----------------------------
DROP TABLE IF EXISTS `cp_tags`;
CREATE TABLE `cp_tags` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tname` varchar(45) NOT NULL DEFAULT '' COMMENT '标签名称',
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='标签表';

-- ----------------------------
-- Records of cp_tags
-- ----------------------------

-- ----------------------------
-- Table structure for cp_users
-- ----------------------------
DROP TABLE IF EXISTS `cp_users`;
CREATE TABLE `cp_users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(60) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` char(32) NOT NULL COMMENT '密码',
  `nickname` varchar(60) NOT NULL DEFAULT '' COMMENT '昵称',
  `u_email` varchar(60) NOT NULL DEFAULT '' COMMENT '登录邮箱',
  `face` varchar(100) NOT NULL DEFAULT '' COMMENT '用户头像',
  `sex` enum('男','女') NOT NULL DEFAULT '男' COMMENT '性别',
  `signature` varchar(100) NOT NULL DEFAULT '' COMMENT '个性签名',
  `last_login_ip` char(16) NOT NULL DEFAULT '0.0.0.0',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '注册时间',
  `user_type` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '1:admin 2:普通会员',
  `last_login_time` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '最后的登录时间',
  `is_black` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否拉黑 1：拉黑 0：正常',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of cp_users
-- ----------------------------

-- ----------------------------
-- Table structure for cp_user_favorites
-- ----------------------------
DROP TABLE IF EXISTS `cp_user_favorites`;
CREATE TABLE `cp_user_favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户 id',
  `title` varchar(255) DEFAULT NULL COMMENT '收藏内容的标题',
  `url` varchar(255) DEFAULT NULL COMMENT '收藏内容的原文地址，不带域名',
  `description` varchar(500) DEFAULT NULL COMMENT '收藏内容的描述',
  `table` varchar(50) DEFAULT NULL COMMENT '收藏实体以前所在表，不带前缀',
  `object_id` int(11) DEFAULT NULL COMMENT '收藏内容原来的主键id',
  `createtime` int(11) DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户收藏表';

-- ----------------------------
-- Records of cp_user_favorites
-- ----------------------------
