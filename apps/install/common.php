<?php
/**
 * 检测目录是否可写
 * @param $d
 * @return bool
 */
function cp_testwrite($d) {
    $tfile = "_test.txt";
    $fp = @fopen($d . "/" . $tfile, "w");
    if (!$fp) {
        return false;
    }
    fclose($fp);
    $rs = @unlink($d . "/" . $tfile);
    if ($rs) {
        return true;
    }
    return false;
}

/**
 * 创建目录
 * @param $path 路径
 * @param int $mode 权限
 * @return bool
 */
function cp_dir_create($path, $mode = 0777) {
    if (is_dir($path)) return true;
    $ftp_enable = 0;
    $path = cp_dir_path($path);
    $temp = explode('/', $path);
    $cur_dir = '';
    $max = count($temp) - 1;
    for ($i = 0; $i < $max; $i++) {
        $cur_dir .= $temp[$i] . '/';
        if (@is_dir($cur_dir)) continue;
        @mkdir($cur_dir, 0777, true);
        @chmod($cur_dir, 0777);
    }
    return is_dir($path);
}

/**
 * @param $path
 * @return mixed|string
 */
function cp_dir_path($path) {
    $path = str_replace('\\', '/', $path);
    if (substr($path, -1) != '/')
        $path = $path . '/';
    return $path;
}
