<?php
namespace app\test\controller;
use think\Controller;
use think\Db;
use think\Session;
use Endroid\QrCode\QrCode;
class Index extends Controller
{
    public function index() {
        return "测试专用";
    }

    //测试发送链接
    public function check_email() {
        //接收key值
        $token = $this->request->param('token');
        $type = $this->request->param('type');
        if ($token && $type) {
            //检测该key是否存在
            $data = Db::name('check_email_sublink')->where(['token'=>$token,'type'=>$type])->find();
            if ($data) {
                $userData = Db::name('admin')->where(array('uid'=>$data['uid']))->field('uid,username')->find();
                //防止重复点击
                if ($data['status']) {
                    if (Session::get('uid')){
                        $this->redirect('/h-index.html');
                    } else {
                        Session::clear();
                        Session::destroy();
                        Session::set('uid',$userData['uid']);
                        Session::set('username',$userData['username']);
                        $this->redirect('/h-index.html');
                    }
                }

                //邮箱注册 激活
                if ($data['type'] == 1 || $data['type'] == 2 || $data['type'] == 3) {
                    $status1 = Db::name('check_email_sublink')->where(['id'=>$data['id']])->setField('status',1);
                    if ($status1) {
                        $this->redirect('/h-win.html');
                    } else {
                        $this->redirect('/h-index.html');
                    }
                }
            } else {
                $this->redirect('/h-index.html');
            }
        } else {
            //不存在token 视为非法操作 直接跳转首页
            $this->redirect('/h-index.html');
        }
    }

    public function win() {
        return '验证成功';
    }

    public function ceshi () {
        $data = \Cocolait\CpEmail::send_email('Cocolait测试','401498334@qq.com',1,'邮箱发送验证码');
    }

    /**
     * 测试 QRcode生成二维码
     * @throws \Endroid\QrCode\Exceptions\DataDoesntExistsException
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionFailedException
     * @throws \Endroid\QrCode\Exceptions\ImageFunctionUnknownException
     * @throws \Endroid\QrCode\Exceptions\ImageTypeInvalidException
     */
    public function qrcodes() {
        $qrCode = new QrCode();
        $qrCode
            ->setText('http://www.mgchen.com/')  // 设置二维码生成的内容
            ->setSize(200)                      //设置二维码图像生成的尺寸
            ->setPadding(10)                    //设置二维码周围的padding值
            ->setErrorCorrection('high')
            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
            ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
            ->setLabel('www.mgchen.com') //设置标题
            ->setLabelFontSize(16)       //设置标题文字字体大小
            ->setLogo('./logo-4.png')   //填充LOGO
            ->setLogoSize(60)           //设置logo图片显示的尺寸
            ->setImageType(QrCode::IMAGE_TYPE_PNG);
        //设置输出头部
        header('Content-Type: '.$qrCode->getContentType());
        // 以http方式 访问直接输出
        $qrCode->render();
    }

    public function qr_index() {
        return $this->fetch('qrcode');
    }
}
