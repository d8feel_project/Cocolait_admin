<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
/*
 * home模块路由文件
 */
use think\Route;
//前台测试模块
Route::group(['prefix' => 'test/Index/', 'ext' => 'html'], function () {
    Route::get('t-index$', 'index');
    Route::get('t-cs$', 'ceshi');
    Route::get('t-qr$', 'qrcodes');
    Route::get('t-show$', 'qr_index');
    Route::get('t-win$', 'win');
    Route::get('t-check-email/:token/:type$', 'check_email');
}, [], ['token' => '[A-Za-z0-9]+','type' => '\d+']);

Route::group(['prefix' => 'test/Syslogin/'], function () {
    Route::get('t-login$', 'index');
    Route::get('t-callback', 'callback');
    Route::get('t-login-type', 'login');
}, [], []);

