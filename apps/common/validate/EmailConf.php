<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2016/12/2
 * Time: 13:45
 * 邮箱配置模块
 */
namespace app\common\validate;
use think\Validate;
class EmailConf extends Validate
{
    //验证规则
    protected $rule =   [
        'send_name'  => 'require',
        'smtp'  => 'require',
        'send_account'  => 'require',
        'send_password'     => 'require',
        'display_name'     => 'require',
        'title'     => 'require',
        'content'     => 'require',
    ];

    //提示信息
    protected $message  =   [
        'send_name.require' => '发件人不能为空 ^_^',
        'smtp.require'   => 'SMTP不能为空 ^_^',
        'send_account.require' => '发件箱帐号不能为空 ^_^',
        'send_password.require' => '发件箱授权密码不能为空 ^_^',
        'display_name.require' => '邮箱模板名称不能为空 ^_^',
        'title.require' => '邮箱标题不能为空 ^_^',
        'content.require' => '邮箱内容不能为空 ^_^',
    ];

    //验证场景
    protected $scene = [
        'edit' => ['send_name','smtp','send_account','send_password'],
        'save_mailbox' => ['display_name','title','content']
    ];

}