<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2016/12/30
 * Time: 13:45
 * 安装模块
 */
namespace app\common\validate;
use think\Validate;
class Install extends Validate
{
    //验证规则
    protected $rule =   [
        'hostname'  => 'require',
        'database'  => 'require',
        'hostport' => 'require',
        'username' => 'require',
        'uname' => 'require',
        'upwd' => 'require|min:6',
        're_upwd' => 'require|confirm:upwd',
        'email' => 'require|email',
    ];

    //提示信息
    protected $message  =   [
        'hostname.require' => '数据库服务器必须填写 ^_^',
        'database.require'   => '数据库名必须填写 ^_^',
        'hostport.require'   => '数据库端口必须填写 ^_^',
        'username.require'   => '数据库用户名必须填写 ^_^',
        'uname.require'   => '管理员帐号必须填写 ^_^',
        'upwd.require' => '管理员密码不能为空 ^_^',
        'upwd.min' => '管理员密码不能小于6个字符 ^_^',
        're_upwd.require' => '管理员确认密码不能为空 ^_^',
        're_upwd.confirm' => '管理员密码和确认密码不一致 ^_^',
        'email.require' => '管理员邮箱不能为空 ^_^',
        'email.email'     => '管理员邮箱格式错误 ^_^',
    ];
}