<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2016/12/2
 * Time: 13:45
 */
namespace app\common\validate;
use think\Validate;
class Admin extends Validate
{
    //验证规则
    protected $rule =   [
        'username'  => 'require|max:10',
        'password'  => 'require|min:6',
        'nickname'  => 'require|max:10',
        'email'     => 'require|email',
        'sex'       => 'require',
        'signature' => 'require',
        'old_password' => 'require|min:6',
        'new_password' => 'require|min:6',
        'repassword' => 'require|confirm:new_password',
    ];

    //提示信息
    protected $message  =   [
        'username.require' => '用户名不能为空 ^_^',
        'username.max'     => '用户名最多不能超过10个字符 ^_^',
        'password.require'   => '密码不能为空 ^_^',
        'password.min'  => '密码不能小于6个字符 ^_^',
        'nickname.require' => '昵称不能为空 ^_^',
        'nickname.max'     => '昵称最多不能超过10个字符 ^_^',
        'email.require' => '邮箱不能为空 ^_^',
        'email.email'     => '邮箱格式错误 ^_^',
        'sex.require' => '性别必须选择哦 ^_^',
        'signature.require' => '个性签名不能为空 ^_^',
        'old_password.require' => '旧密码不能为空 ^_^',
        'old_password.min' => '旧密码不能小于6个字符 ^_^',
        'new_password.require' => '新密码不能为空 ^_^',
        'new_password.min' => '新密码不能小于6个字符 ^_^',
        'repassword.require' => '确认密码不能为空 ^_^',
        'repassword.confirm' => '新密码和确认密码不一致 ^_^',
    ];

    //验证场景
    protected $scene = [
        'login' => ['username','password'],
        'edit_info' => ['nickname','email','sex','signature'],
        'edit_pwd' => ['old_password','new_password','repassword'],
    ];

}