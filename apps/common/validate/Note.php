<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2016/12/2
 * Time: 13:45
 * 短信配置模块
 */
namespace app\common\validate;
use think\Validate;
class Note extends Validate
{
    //验证规则
    protected $rule =   [
        'appkey'  => 'require',
        'secretKey'  => 'require',
        'free_sign_name' => 'require',
        'template_code' => 'require',
        'display_name' => 'require',
        'scene' => 'require',
    ];

    //提示信息
    protected $message  =   [
        'appkey.require' => 'appkey必须填写 ^_^',
        'secretKey.require'   => 'secretKey必须填写 ^_^',
        'free_sign_name.require'   => '短信签名必须填写 ^_^',
        'template_code.require'   => '短信模板ID必须填写 ^_^',
        'display_name.require'   => '短信模板名称必须填写 ^_^',
        'scene.require'   => '验证短信场景标识必须填写 ^_^',
    ];

    //验证场景
    protected $scene = [
        'send' => ['appkey','secretKey'],
        'edit' => ['free_sign_name','template_code','display_type','scene']
    ];

}