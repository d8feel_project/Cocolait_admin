<?php
//后台基类
namespace app\common\controller;
use think\Controller;
use think\Db;
use think\Session;
use Gregwar\Image\Image;
use Sirius\Upload\Handler as UploadHandler;
class Base extends Controller
{
    public function _initialize()
    {
        if (!Session::get('uid') && !Session::get('username')) {
            $this->redirect('/a-login.html');
        }
        $this->assign([
            'adminMenu' => config('menu.adminMenu'),
            'adminFindData' => Db::name('admin')->where(['uid'=>Session::get('uid')])->field(['last_login_ip','last_login_time'])->find(),
            'mysql_version' => $this->_mysql_version(),
        ]);
    }

    /**
     * 获取mysql版本
     * @return mixed
     */
    private function _mysql_version() {
        $version = Db::query("select version() as ver");
        return $version[0]['ver'];
    }


    /**
     * 文件处理 （composer引入第三方包）
     * @return array
     * @throws \Exception
     */
    protected function upload_file() {
        $dir = "./uploads/face" . "/" . date('Ymd');
        if (!file_exists($dir)) {
            if (!mkdir($dir,0777,true)) {
                return ['error'=>'1','msg'=>"无法创建目录：" . $dir];
            };
        }
        $uploadHandler = new UploadHandler($dir);
        $uploadHandler->addRule('extension', ['allowed' => ['jpeg', 'png','jpg','gif']], '只能上传后缀为(.jpg, .jpeg, .png, .gif)文件');
        $uploadHandler->addRule('size', ['max' => '5M'], '文件最大上传为5M');
        $result = $uploadHandler->process($_FILES['up_face']);

        if ($result->isValid()) {
            try {
                $result->confirm(); // 删除后缀.lock文件
                $img = $dir . "/" . $result->name;
                // 图片裁剪
                $image = Image::open($img);
                $data = [
                    'face' => $dir . "/" . "mini_" . $result->name,
                    'face_240' => $dir . "/" . "max_" . $result->name,
                ];
                $image->forceResize(240,180)->save($data['face_240']);
                $image->forceResize(50,50)->save($data['face']);
                //删除旧图片
                unlink($img);
                $where = ['uid' => Session::get('uid')];
                $oldData = Db::name('admin')->where($where)->field(['face','face_240_180'])->find();
                if ($oldData['face']) {
                    if (file_exists("." . $oldData['face'])) {
                        unlink("." . $oldData['face']);
                    }
                    if (file_exists("." . $oldData['face_240_180'])) {
                        unlink("." . $oldData['face_240_180']);
                    }
                }
                Db::name('admin')->where($where)
                ->update(['face'=>substr($data['face'],1),'face_240_180'=>substr($data['face_240'],1)]);
                // 返回数据
                return ['error' => 0,'msg'=>'上传成功 ^_^','url' => substr($data['face_240'],1)];
            } catch (\Exception $e) {
                $result->clear();
                throw $e;
            }
        } else {
            $message = $result->getMessages();
            foreach ($message as $v) {
                $error = $v->template;
            }
            return ['error' => 1,'msg'=> $error];
        }
    }

}
