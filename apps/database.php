<?php return [
  'type' => 'mysql',
  'hostname' => '127.0.0.1',
  'username' => 'root',
  'password' => 'root',
  'hostport' => '3306',
  'database' => 'tk_admin',
  'prefix' => 'cp_',
  'sql_explain' => false,
  'auto_timestamp' => false,
  'resultset_type' => 'array',
  'fields_strict' => true,
  'slave_no' => '',
  'master_num' => 1,
  'rw_separate' => false,
  'deploy' => 0,
  'debug' => true,
  'params' => 
  [
  ],
  'dsn' => '',
  'charset' => 'utf8',
];