<?php
/**
 * 第三方登录配置项
 * Created by PhpStorm.
 * User: chen
 * Date: 2017/1/13
 * Time: 14:15
 */
return [
    'think_sdk_qq' => [
        'app_key' => '',
        'app_secret' => '',
        'scope'      => 'get_user_info',
        'display' => 'default',
        'callback'   => [
            'default' => 'http://www.mgchen.com/t-callback/type/qq',
            'mobile'  => 'http://www.mgchen.com/t-callback/type/qq',
        ]
    ],
    'think_sdk_sina' => [
        'app_key' => '',
        'app_secret' => '',
        'scope'      => 'get_user_info',
        'callback'   => [
            'default' => '',
            'mobile'  => '',
        ]
    ],
    'think_sdk_weixin' => [
        'app_key' => '',
        'app_secret' => '',
        'scope'      => 'get_user_info',
        'callback'   => [
            'default' => '',
            'mobile'  => '',
        ]
    ],
    'think_sdk_wechat' => [
        'app_key' => '',
        'app_secret' => '',
        'scope'      => 'get_user_info',
        'callback'   => [
            'default' => '',
            'mobile'  => '',
        ]
    ],
];