<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2016/12/2
 * Time: 9:49
 */
return [
    'adminMenu' => [
        //首页
        ['module'=>'admin','controller'=>'Index','open'=>'has-sub active','close' =>'has-sub',
            'live_2'=> [
                [
                    'name'=>'首页',
                    'controller'=>'',
                    'action'=>'',
                    'ico'=>'fa fa-laptop',
                    'newName'=>'',
                    'live_3'=>[
                        [
                            'name'=>'首页',
                            'controller'=>'Index',
                            'action'=>'index',
                            'url'=>'/',
                        ],

                    ]
                ]
            ]
        ],
        //设置
        ['module'=>'admin','controller'=>'Setting','open'=>'has-sub active','close' =>'has-sub',
            'live_2'=> [
                [
                    'name'=>'基本设置',
                    'controller'=>'',
                    'action'=>'',
                    'ico'=>'fa fa-cog',
                    'newName'=>'New',
                    'live_3'=>[
                        [
                            'name'=>'个人信息',
                            'controller'=>'Setting',
                            'action'=>'userinfo',
                            'url'=>'/a-info.html',
                        ],
                        [
                            'name'=>'网站信息',
                            'controller'=>'',
                            'action'=>'',
                            'url'=>'',
                        ],
                        [
                            'name'=>'上传设置',
                            'controller'=>'',
                            'action'=>'',
                            'url'=>'',
                        ],
                    ]
                ]
            ]
        ],
        //邮箱配置
        ['module'=>'admin','controller'=>'Email','open'=>'has-sub active','close' =>'has-sub',
            'live_2'=> [
                [
                    'name'=>'邮箱设置',
                    'controller'=>'',
                    'action'=>'',
                    'ico'=>'fa fa-envelope',
                    'newName'=>'',
                    'live_3'=>[
                        [
                            'name'=>'SMTP配置',
                            'controller'=>'Email',
                            'action'=>'index',
                            'url'=>'/a-email.html',
                        ],
                        [
                            'name'=>'邮箱模板列表',
                            'controller'=>'Email',
                            'action'=>'mailbox_list',
                            'url'=>'/em-mailbox-list.html',
                        ],
                        [
                            'name'=>'注册邮箱模板',
                            'controller'=>'Email',
                            'action'=>'mailbox',
                            'url'=>'/em-mailbox.html',
                        ],
                    ]
                ]
            ]
        ],
        //短信配置
        ['module'=>'admin','controller'=>'Note','open'=>'has-sub active','close' =>'has-sub',
            'live_2'=> [
                [
                    'name'=>'Alidayu短信设置',
                    'controller'=>'',
                    'action'=>'',
                    'ico'=>'fa fa-rss-square',
                    'newName'=>'',
                    'live_3'=>[
                        [
                            'name'=>'SMS配置',
                            'controller'=>'Note',
                            'action'=>'index',
                            'url'=>'/nt-show.html',
                        ],
                        [
                            'name'=>'短信模板列表',
                            'controller'=>'Note',
                            'action'=>'note_list',
                            'url'=>'/nt-list.html',
                        ],
                        [
                            'name'=>'注册短信模板',
                            'controller'=>'Note',
                            'action'=>'note_display',
                            'url'=>'/nt-theme.html',
                        ],
                    ]
                ]
            ]
        ],
        //Excel应用管理
        ['module'=>'admin','controller'=>'Excel','open'=>'has-sub active','close' =>'has-sub',
            'live_2'=> [
                [
                    'name'=>'Excel应用管理',
                    'controller'=>'',
                    'action'=>'',
                    'ico'=>'fa fa-folder-open',
                    'newName'=>'',
                    'live_3'=>[
                        [
                            'name'=>'导入Excel文件',
                            'controller'=>'Excel',
                            'action'=>'index',
                            'url'=>'/ex-show.html',
                        ],
                        [
                            'name'=>'导出Excel文件',
                            'controller'=>'Excel',
                            'action'=>'halt_file',
                            'url'=>'/ex-halt.html',
                        ],
                        [
                            'name'=>'Excel列表',
                            'controller'=>'Excel',
                            'action'=>'excel_list',
                            'url'=>'/ex-list.html',
                        ],
                        [
                            'name'=>'Excel转换体验',
                            'controller'=>'Excel',
                            'action'=>'excel_display',
                            'url'=>'/ex-theme.html',
                        ],
                    ]
                ]
            ]
        ],
        //QRcode二维码
        ['module'=>'admin','controller'=>'Qrcodes','open'=>'has-sub active','close' =>'has-sub',
            'live_2'=> [
                [
                    'name'=>'QRcode二维码',
                    'controller'=>'',
                    'action'=>'',
                    'ico'=>'fa fa-css3',
                    'newName'=>'',
                    'live_3'=>[
                        [
                            'name'=>'QRcode动态生成',
                            'controller'=>'Qrcodes',
                            'action'=>'index',
                            'url'=>'/qr-show.html',
                        ]
                    ]
                ]
            ]
        ],
    ],
];