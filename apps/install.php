<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
/*
 * install 模块路由文件
 */
use think\Route;
Route::get('sp1$','install/Index/index',['ext'=>'html']);
Route::get('sp2/[:id]$','install/Index/step2',['ext'=>'html']);
Route::get('sp3/[:token]$','install/Index/step3',['ext'=>'html']);
Route::post('ajax_sp$','install/Index/ajax_step',['ext'=>'html']);
