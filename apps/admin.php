<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
/*
 * admin模块路由文件
 */
use think\Route;
//首页模块
Route::group(['prefix' => 'admin/Index/', 'ext' => 'html'], function () {
    Route::get('a-index$', 'index');
}, [], []);
//登录模块
Route::group(['prefix' => 'admin/Login/', 'ext' => 'html'], function () {
    Route::get('a-loginOut$', 'login_out');
    Route::get('a-login$', 'index');
    Route::post('a-send_login$', 'send_login');
}, [], []);
//设置模块
Route::group(['prefix' => 'admin/Setting/', 'ext' => 'html'], function () {
    Route::get('a-info$', 'userinfo');
    Route::post('a-save_info$', 'save_info');
    Route::post('a-edit_pwd$', 'edit_pwd');
    Route::post('a-list-info$', 'list_info');
    Route::post('a-upload$', 'up_file');
    Route::post('a-del-img$', 'del_img_file');
}, [], []);
//邮箱配置模块
Route::group(['prefix' => 'admin/Email/', 'ext' => 'html'], function () {
    Route::get('a-email$', 'index');
    Route::get('em-mailbox/[:id]$', 'mailbox');
    Route::get('em-mailbox-list$', 'mailbox_list');
    Route::post('a-email-conf$', 'save_email_conf');
    Route::post('em-find_list$', 'get_find_list');
    Route::post('em-save-mailbox$', 'save_mailbox');
    Route::post('em-del-mailbox$', 'del_mailbox_list');
}, [], ['id' => '\d+']);
//Alidayu短信配置模块
Route::group(['prefix' => 'admin/Note/', 'ext' => 'html'], function () {
    Route::get('nt-show$', 'index');
    Route::get('nt-theme/[:id]$', 'note_display');
    Route::get('nt-list$', 'note_list');
    Route::post('nt-save-conf$', 'save_note_conf');
    Route::post('nt-get-conf$', 'get_find_note');
    Route::post('nt-save-theme$', 'save_note_theme');
    Route::post('nt-del$', 'note_del');
    Route::post('nt-find$', 'note_get_find');
}, [], ['id' => '\d+']);
//Excel应用管理模块
Route::group(['prefix' => 'admin/Excel/', 'ext' => 'html'], function () {
    Route::get('ex-show$', 'index');
    Route::get('ex-halt$', 'halt_file');
    Route::get('ex-theme/[:id]$', 'excel_display');
    Route::get('ex-list$', 'excel_list');
    Route::get('ex-download/[:id]$', 'ajax_download');
    Route::post('ex-upload$', 'uploads');
    Route::post('ex-del$', 'del_file');
}, [], ['id' => '\d+']);
//QRcode二维码
Route::group(['prefix' => 'admin/Qrcodes/', 'ext' => 'html'], function () {
    Route::get('qr-show$', 'index');
    Route::get('qr-act/[:t]$', 'qrcode_act_show');
    Route::get('qr-general/[:t]$', 'qrcode_general_show');
}, [], []);

