<?php
namespace app\admin\controller;
use app\common\controller\Base;
use think\Db;
/**
 * 邮箱配置
 * Class Email
 * @package app\admin\controller
 */
class Note extends Base
{
    /**
     * SMS配置
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch('index',[
            'crumbs'=>[
                ['item'=>0,'url' => '/','name'=>'首页'],
                ['item'=>0,'url' => '/a-email.html','name'=>'Alidayu短信设置'],
                ['item'=>1,'url' => '','name'=>'SMS配置']
            ]
        ]);
    }

    /**
     * 短信配置
     * @return array|\think\response\Json
     * @throws \think\Exception
     */
    public function save_note_conf() {
        if ($this->request->isAjax()) {
            $data = $this->request->param();
            $validate = new \app\common\validate\Note();
            if (!$requst = $validate->scene('send')->check($data)) {
                return ['error'=>1,'msg'=>$validate->getError()];
            }
            $save_data = [
                'appkey' => trim($data['appkey']),
                'secretKey' => trim($data['secretKey']),
                'format' => empty($data['format']) ? 'json' : $data['format']
            ];
            if ($data['init_id']) {
                //修改
                Db::name('options')->where(['option_id' => $data['init_id']])->update([
                    'option_value' => json_encode($save_data)
                ]);
                return ['error'=>0,'msg'=>'保存成功 ^_^'];
            } else {
                //添加
                unset($data['init_id']);
                $int = Db::name('options')->insert([
                    'option_name' => '短信配置',
                    'option_value' => json_encode($save_data),
                    'autoload' => 1,
                    'intro' => '阿里大鱼短信配置'
                ]);
                if ($int) {
                    return ['error'=>0,'msg'=>'保存成功 ^_^'];
                } else {
                    return ['error'=>1,'msg'=>'系统繁忙,稍后再试...'];
                }
            }
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }

    /**
     * 获取短信配置数据
     * @return array|\think\response\Json
     */
    public function get_find_note() {
        if ($this->request->isAjax()) {
            $result  = Db::name('options')->where(['option_name'=>'短信配置'])->field(['option_id','option_value'])->find();
            if ($result) {
                $data = json_decode($result['option_value']);
                $id = $result['option_id'];
            } else {
                $data = [];
                $id = 0;
            }
            return ['error'=>0,'msg'=>'','data'=> $data,'id'=> $id];
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }

    /**
     * 注册短信模板
     * @return mixed
     */
    public function note_display() {
        $init_id = $this->request->param('id',0,'int');
        if ($init_id) {
            $is_status = Db::name('send_note')->where(['id'=>$init_id])->value('is_status');
        }
        return $this->fetch('note_display',[
            'crumbs'=>[
                ['item'=>0,'url' => '/','name'=>'首页'],
                ['item'=>0,'url' => '/a-email.html','name'=>'Alidayu短信设置'],
                ['item'=>1,'url' => '','name'=>'配置短信模板']
            ],
            'init_id' => $init_id,
            'is_status' => isset($is_status) ? $is_status : ''
        ]);
    }

    /**
     * 注册短信模板
     * @return mixed
     */
    public function note_get_find() {
        if ($this->request->isAjax()) {
            $id = $this->request->param('id',0,'intval');
            $result = Db::name('send_note')->where(['id'=>$id])->find();
            return ['error'=>0,'msg'=>'','data'=>$result];
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }

    /**
     * 注册短信模板 保存 | 修改
     * @return array|\think\response\Json
     * @throws \think\Exception
     */
    public function save_note_theme() {
        if ($this->request->isAjax()) {
            $data = $this->request->param();
            $validate = new \app\common\validate\Note();
            if (!$requst = $validate->scene('edit')->check($data)) {
                return ['error'=>1,'msg'=>$validate->getError()];
            }
            $insert_data = [
                'free_sign_name' => trim($data['free_sign_name']),
                'template_code' => trim($data['template_code']),
                'scene' => trim($data['scene']),
                'display_name' => trim($data['display_name'])
            ];
            if ($data['is_status'] === 'true') {
                $insert_data['is_status'] = 1;
            } else {
                $insert_data['is_status'] = 0;
            }
            $model = new \app\admin\model\SendNote;
            if (!$model->check_field($data['id'],$insert_data)) {
                return ['error'=>1,'msg' => $model->getError()];
            } else {
                return ['error'=>0,'msg'=>'保存成功 ^_^','url'=>'/nt-list.html'];
            }
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }


    /**
     * 配置短信模板-列表
     * @return mixed
     */
    public function note_list() {
        return $this->fetch('note_list',[
            'crumbs'=>[
                ['item'=>0,'url' => '/','name'=>'首页'],
                ['item'=>0,'url' => '/a-email.html','name'=>'Alidayu短信设置'],
                ['item'=>1,'url' => '','name'=>'短信模板列表']
            ],
            'list_data' => Db::name('send_note')->select()
        ]);
    }


    /**
     * 配置短信模板-列表 - ajax删除短信模板
     * @return mixed
     */
    public function note_del() {
        if ($this->request->isAjax()) {
            $id = $this->request->param('id');
            if (!$id) return ['error'=>1,'msg' => '参数错误 ^_^'];
            if (Db::name('send_note')->where(['id'=>$id])->delete()) {
                return ['error'=>0,'msg' => '删除成功 ^_^'];
            } else {
                return ['error'=>1,'msg' => '系统繁忙,稍后再试...'];
            }
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }
}
