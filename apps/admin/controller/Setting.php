<?php
namespace app\admin\controller;
use app\common\controller\Base;
use think\Db;
use think\Session;
/**
 * 设置模块
 * Class Setting
 * @package app\admin\controller
 */
class Setting extends Base
{
    /**
     * 个人信息
     * @return \think\Response
     */
    public function userinfo()
    {
        return $this->fetch('userinfo',
            [
                'crumbs'=>[
                    ['item'=>0,'url' => '/','name'=>'首页'],
                    ['item'=>0,'url' => '/a-info.html','name'=>'设置'],
                    ['item'=>1,'url' => '','name'=>'个人信息']
                ]
            ]
        );
    }

    /**
     * 修改个人信息
     * @return array|\think\response\Json
     */
    public function save_info() {
        if ($this->request->isAjax()) {
            $model = new \app\admin\model\Admin();
            if ($model->save_info()) {
                return ['error'=>0,'msg'=>'保存成功 ^_^'];
            } else {
                return ['error'=>1,'msg'=>$model->getError()];
            }
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }

    /**
     * 修改密码
     * @return array|\think\response\Json
     */
    public function edit_pwd() {
        if ($this->request->isAjax()) {
            $model = new \app\admin\model\Admin();
            if ($model->edit_pwd()) {
                return ['error'=>0,'msg'=>'修改成功 ^_^','url'=>'/a-loginOut.html'];
            } else {
                return ['error'=>1,'msg'=>$model->getError()];
            }
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }

    /**
     * 获取数据
     * @return array|\think\response\Json
     */
    public function list_info() {
        if ($this->request->isAjax()) {
            $model = new \app\admin\model\Admin();
            if ($data = $model->get_find_data(['uid'=>Session::get('uid')])) {
                return ['error'=>0,'msg'=>'','data'=>$data];
            } else {
                return ['error'=>1,'msg'=>'获取失败'];
            }
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }

    /**
     * 上传头像
     */
    public function up_file() {
        return json($this->upload_file());
    }

    /**
     * 移除图片数据
     * @return array|\think\response\Json
     */
    public function del_img_file() {
        if ($this->request->isAjax()) {
            $data = $this->request->param();
            if (!$data) return ['error'=>0,'msg'=>'参数错误,请重试...'];
            @unlink("." . $data['face']);
            @unlink("." . $data['face_240']);
            Db::name('admin')->where(['uid'=>Session::get('uid')])->update(['face'=>'','face_240_180'=>'']);
            return ['error'=>0,'msg'=>'移除成功 ^_^'];
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }
}
