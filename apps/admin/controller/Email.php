<?php
namespace app\admin\controller;
use app\common\controller\Base;
use think\Db;
/**
 * 邮箱配置
 * Class Email
 * @package app\admin\controller
 */
class Email extends Base
{
    /**
     * SMTP配置
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch('index',[
            'crumbs'=>[
                ['item'=>0,'url' => '/','name'=>'首页'],
                ['item'=>0,'url' => '/a-email.html','name'=>'邮箱设置'],
                ['item'=>1,'url' => '','name'=>'SMTP配置']
            ]
        ]);
    }

    /**
     * 邮箱模板
     *
     * @return \think\Response
     */
    public function mailbox()
    {
        $id = $this->request->param('id',0,'intval');
        $result = Db::name('email_display')->field(['display_name','content','is_status'])->where(['id'=>$id])->find();
        //初始化分配的数据
        if ($result) {
            $con_data = json_decode($result['content'],true);
            $data = [
                'display_name' => $result['display_name'],
                'title' => $con_data['title'],
                'content' => $con_data['content'],
                'is_status' => $result['is_status']
            ];
        } else {
            $data = [
                'display_name' => '',
                'title' => '',
                'content' => '',
                'is_status' => '-1'
            ];
        }
        return $this->fetch('mailbox',[
            'crumbs'=>[
                ['item'=>0,'url' => '/','name'=>'首页'],
                ['item'=>0,'url' => '/a-email.html','name'=>'邮箱设置'],
                ['item'=>1,'url' => '','name'=>'邮箱模板']
            ],
            'find_data' => $data,
            'id' => $id,
        ]);
    }

    /**
     * 邮箱模板列表
     *
     * @return \think\Response
     */
    public function mailbox_list()
    {
        return $this->fetch('mailbox_list',[
            'crumbs'=>[
                ['item'=>0,'url' => '/','name'=>'首页'],
                ['item'=>0,'url' => '/a-email.html','name'=>'邮箱设置'],
                ['item'=>1,'url' => '','name'=>'邮箱模板列表']
            ],
            'list_data' => Db::name('email_display')->field(['id','display_name','is_status'])->select()
        ]);
    }

    /**
     * 修改 或者 添加 SMTP配置
     * @return array|\think\response\Json
     */
    public function save_email_conf() {
        if ($this->request->isAjax()) {
            $data = $this->request->param();
            $validate = new \app\common\validate\EmailConf();
            if (!$requst = $validate->scene('edit')->check($data)) {
                return ['error'=>1,'msg'=>$validate->getError()];
            }
            if ($data['init_id']) {
                //修改
                Db::name('options')->where(['option_id'=>$data['init_id']])->update([
                    'option_value' => json_encode($data)
                ]);
                return ['error'=>0,'msg'=>'保存成功 ^_^'];
            } else {
                //添加
                unset($data['init_id']);
                $int = Db::name('options')->insert([
                    'option_name' => '邮箱配置',
                    'option_value' => json_encode($data),
                    'autoload' => 1,
                    'intro' => '邮箱配置文件'
                ]);
                if ($int) {
                    return ['error'=>0,'msg'=>'保存成功 ^_^'];
                } else {
                    return ['error'=>1,'msg'=>'系统繁忙,稍后再试...'];
                }
            }
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }

    /**
     * 获取配置数据
     * @return array|\think\response\Json
     */
    public function get_find_list() {
        if ($this->request->isAjax()) {
            $data = Db::name('options')->where(['option_name'=>'邮箱配置'])->find();
            if (!$data) {
                $data = [];
                $data['option_id'] = 0;
                $data['option_value'] = '';
            }
            return ['error'=>0,'msg'=>'','data'=>json_decode($data['option_value']),'id'=>$data['option_id']];
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }

    /**
     * 保存 注册邮箱模板
     * @return \think\response\Json
     */
    public function save_mailbox() {
        if ($this->request->isAjax()) {
            $data = $this->request->param();
            //验证器 验证字段
            $validate = new \app\common\validate\EmailConf();
            if (!$requst = $validate->scene('save_mailbox')->check($data)) {
                return ['error'=>1,'msg'=>$validate->getError()];
            }
            $display_name = trim($data['display_name']);
            if ($data['id']) {
                //检测除自己本身以外所有模板名称
                $d_name = Db::name('email_display')->where(['display_name' => $display_name,'id' => ['<>',$data['id']]])->select();
                if ($d_name) {
                    return ['error'=>1,'msg'=> '该模板名称已被使用,请更换 ^_^'];
                }
            } else {
                if (Db::name('email_display')->where(['display_name' => $display_name])->value('id')) {
                    return ['error'=>1,'msg'=> '该模板名称已被使用,请更换 ^_^'];
                }
            }
            $con_data = [
                'title' => trim($data['title']),
                'content' => $data['content']
            ];
            $save_data = [
                'display_name' => $display_name,
                'content' => json_encode($con_data),
                'is_status' => isset($data['is_status']) ? '1' : '0'
            ];
            if ($data['id']) {
                //修改
                Db::name('email_display')->where(['id'=>$data['id']])->update($save_data);
                return ['error'=>0,'msg'=>'保存成功,马上为您跳转 ^_^','url'=>'/em-mailbox-list.html'];
            } else {
                //添加
                if (Db::name('email_display')->insert($save_data)) {
                    return ['error'=>0,'msg'=>'保存成功,马上为您跳转 ^_^','url'=>'/em-mailbox-list.html'];
                } else {
                    return ['error'=>1,'msg'=>'系统繁忙,稍后再试...'];
                }
            }

        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }

    /**
     * 删除指定的邮箱模板
     * @return array|\think\response\Json
     * @throws \think\Exception
     */
    public function del_mailbox_list() {
        if ($this->request->isAjax()) {
            $id = $this->request->param('id');
            if (!$id) return ['error'=>1,'msg' => '参数错误 ^_^'];
            if (Db::name('email_display')->where(['id'=>$id])->delete()) {
                return ['error'=>0,'msg' => '删除成功 ^_^'];
            } else {
                return ['error'=>1,'msg' => '系统繁忙,稍后再试...'];
            }
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }
}
