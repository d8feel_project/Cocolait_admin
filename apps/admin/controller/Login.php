<?php
namespace app\admin\controller;
use think\Controller;
use app\admin\model\Admin;
use think\Session;

/**
 * 后台登录
 * Class
 * @package app\admin\controllerlogin
 */
class login extends Controller
{
    public function index() {
        if (Session::get('errorData')) {
            Session::delete('errorData');
            Session::flush();
        }
        return $this->fetch();
    }

    //执行登录
    public function send_login() {
        if ($this->request->isAjax()) {
            $admin_model = new Admin();
            if ($admin_model->send_login()) {
                return ['error'=>0,'msg'=>'','url'=>'/a-index.html'];
            } else {
                return ['error'=>1,'msg'=>$admin_model->getError()];
            }
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }

    }

    //退出登录
    public function login_out() {
        Session::flush();
        Session::clear();
        $this->redirect('/a-login.html');
    }
}
