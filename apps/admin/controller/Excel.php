<?php
namespace app\admin\controller;
use app\common\controller\Base;
use Cocolait;
use think\Db;
/**
 * Excel 处理
 * Class Excel
 * @package app\admin\controller
 */
class Excel extends Base
{
    /**
     * 导入Excel文件
     * @return mixed
     */
    public function index() {
        return $this->fetch('index',[
            'crumbs'=>[
                ['item'=>0,'url' => '/','name'=>'首页'],
                ['item'=>0,'url' => '/ex-list.html','name'=>'Excel应用管理'],
                ['item'=>1,'url' => '','name'=>'导入Excel文件']
            ]
        ]);
    }

    /**
     * Excel转换体验
     * @return mixed
     */
    public function excel_display() {
        $id = $this->request->param('id',0,'int');
        $data = [];
        if ($id) {
            $data = Db::name('excel')->where(['id'=>$id])->field(['name','url'])->find();
            $ext = substr(strrchr($data['name'],'.'),1);
            $data = Cocolait\CpExcel::excelFileToArray($data['url'],$ext);
//            return json($data);
//            $res = [];
            /*foreach ($data as $k => $v) {
                foreach ($v as $k2 => $v2) {
                    if (!empty($v2)) {
                        $res[$k][$k2] = $v2;
                    }
                }
            }*/
            return $this->fetch('test',[
                'crumbs'=>[
                    ['item'=>0,'url' => '/','name'=>'首页'],
                    ['item'=>0,'url' => '/ex-list.html','name'=>'Excel应用管理'],
                    ['item'=>1,'url' => '','name'=>'Excel转换体验']
                ],
                'data' => $data
            ]);
        } else {
            return $this->fetch('excel_display',[
                'crumbs'=>[
                    ['item'=>0,'url' => '/','name'=>'首页'],
                    ['item'=>0,'url' => '/ex-list.html','name'=>'Excel应用管理'],
                    ['item'=>1,'url' => '','name'=>'Excel转换体验']
                ],
                'data' => $data
            ]);
        }
    }

    /**
     * Excel列表
     * @return mixed
     */
    public function excel_list() {
        return $this->fetch('excel_list',[
            'crumbs'=>[
                ['item'=>0,'url' => '/','name'=>'首页'],
                ['item'=>0,'url' => '/ex-list.html','name'=>'Excel应用管理'],
                ['item'=>1,'url' => '','name'=>'Excel列表']
            ],
            'list_data' => Db::name('excel')->select(),
        ]);
    }

    /**
     * 上传Excel文件
     * @return \think\response\Json
     * @throws \Exception
     */
    public function uploads() {
        $data = Cocolait\CpExcel::importExcel();
        return json($data);
    }

    /**
     * 下载文件
     * @return array|\think\response\Json
     */
    public function ajax_download() {
        $id = $this->request->param('id');
        $url = Db::name('excel')->where(['id'=>$id])->value('url');
        Cocolait\CpDownload::download($url);
    }

    /**
     * 删除文件
     * @return array|\think\response\Json
     * @throws \think\Exception
     */
    public function del_file() {
        if ($this->request->isAjax()) {
            $id = $this->request->param('id');
            if (!$id) return ['error'=>1,'msg'=>'参数错误'];
            $url = Db::name('excel')->where(['id'=>$id])->value('url');
            if ($url) {
                unlink($url);
            }
            Db::name('excel')->where(['id'=>$id])->delete();
            return ['error'=>0,'msg'=>'删除成功 ^_^'];
        } else {
            return json(['error'=>1,'msg'=>'404'],404);
        }
    }

    /**
     * 导出 Excel文件
     */
    public function halt_file() {
        $fileName = '阿里大鱼短信模板配置';
        $testData = Db::name('send_note')->select();
        $excelColumnItem = ['ID','短信签名','短信模板ID','验证场景标识','短信模板名称','启用状态'];
        $data = [];
        foreach ($testData as $k => $v){
            $data[$k]['id'] = $v['id'];
            $data[$k]['free_sign_name'] = $v['free_sign_name'];
            $data[$k]['template_code'] = $v['template_code'];
            $data[$k]['scene'] = $v['scene'];
            $data[$k]['display_name'] = $v['display_name'];
            $data[$k]['is_status'] = ($v['is_status'] == 1) ? '正常' : '关闭';
        }
       \Cocolait\CpExcel::exportExcel($fileName,$excelColumnItem,$data);
    }
}
