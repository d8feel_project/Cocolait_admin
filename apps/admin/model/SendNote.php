<?php
/**
 * 短信模板
 */
namespace app\admin\model;
use think\Model;
class SendNote extends Model
{
    //检测字段
    public function check_field($id,$data) {
        if ($id) {
            //修改
            if ($this->where(['template_code'=>$data['template_code'],'id'=>['<>',$id]])->select()) {
                $this->error = '短信模板ID已被使用';
                return false;
            }
            if ($this->where(['scene'=>$data['scene'],'id'=>['<>',$id]])->select()) {
                $this->error = '场景标识已被使用';
                return false;
            }
            if ($this->where(['display_name'=>$data['display_name'],'id'=>['<>',$id]])->select()) {
                $this->error = '短信模板名称已被使用';
                return false;
            }
            $this->isUpdate(true)->save($data,['id'=>$id]);
            return true;
        } else {
            //添加
            if ($this->where(['template_code'=>$data['template_code']])->value('id')) {
                $this->error = '短信模板ID已被使用';
                return false;
            }
            if ($this->where(['scene'=>$data['scene']])->value('id')) {
                $this->error = '场景标识已被使用';
                return false;
            }
            if ($this->where(['display_name'=>$data['display_name']])->value('id')) {
                $this->error = '短信模板名称已被使用';
                return false;
            }
            $this->isUpdate(false)->save($data);
            return true;
        }
    }
}
