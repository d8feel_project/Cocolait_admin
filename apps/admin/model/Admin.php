<?php
namespace app\admin\model;
use think\Model;
use think\Request;
use think\Session;
class Admin extends Model
{
    //指定主键
    protected $pk = 'uid';

    //自定义初始化
    protected function initialize()
    {
        parent::initialize();
    }

    //执行登录
    public function send_login() {
        $request = Request::instance();
        $validate = new \app\common\validate\Admin();
        $data = $request->param();
        if (!$requst = $validate->scene('login')->check($data)) {
            $this->error = $validate->getError();
            return false;
        }
        $where = ['username' => $data['username']];
        $objData = $this->where($where)->find();
        if ($objData->is_lock) {
            $this->error = '用户已被锁定,请联系管理员!';
            return false;
        }
        if ($objData->pwd_error_num >= 10) {
            $this->where(['uid'=>$objData->uid])->setField('is_lock',1);
            $this->error = '用户已被锁定,请联系管理员!';
            return false;
        }
        if (!$objData) {
            $this->error = '用户名或密码错误 ^_^';
            return false;
        }
        if (!compare_password($data['password'],$objData->password)) {
            $this->where(['uid'=>$objData->uid])->setInc('pwd_error_num');
            $this->error = '用户名或密码错误 ^_^';
            return false;
        }
        $this->isUpdate(true)->save(['last_login_ip'=>\Cocolait\CpGet::get_client_ip(0,true),'last_login_time'=>time()],['uid'=>$objData->uid]);
        Session::set('uid',$objData->uid);
        Session::set('username',$objData->username);
        return true;
    }

    //保存信息
    public function save_info() {
        $request = Request::instance();
        $validate = new \app\common\validate\Admin();
        $data = $request->param();
        if (!$requst = $validate->scene('edit_info')->check($data)) {
            $this->error = $validate->getError();
            return false;
        }
        $checkEmail = $this->where(['email'=>$data['email'],'uid'=>['<>',Session::get('uid')]])->select();
        if ($checkEmail) {
            $this->error = '该邮箱已被占用 ^_^';
            return false;
        }
        $this->isUpdate(true)->save($data,['uid'=>Session::get('uid')]);
        return true;
    }

    //修改密码
    public function edit_pwd () {
        $request = Request::instance();
        $validate = new \app\common\validate\Admin();
        $data = $request->param();
        if (!$requst = $validate->scene('edit_pwd')->check($data)) {
            $this->error = $validate->getError();
            return false;
        }
        $password = $this->where(['uid'=>Session::get('uid')])->value('password');
        if (!compare_password($data['old_password'],$password)) {
            $this->error = '旧密码错误 ^_^';
            return false;
        }
        $this->isUpdate(true)->save(['password'=>encrypt_password($data['new_password'])],['uid'=>Session::get('uid')]);
        return true;
    }

    //获取单一的数据
    public function get_find_data($where = [],$field = ['uid','nickname','sex','email','signature','face_240_180','face','username']) {
        return $this->field($field)->where($where)->find()->toArray();
    }
}
